//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------
#ifndef EMMA_H
#define EMMA_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <termcap.h>
#include <termios.h>
#include <stdarg.h>
#include <dirent.h>
#include <errno.h>
#include <libgen.h>
#include <ctype.h>
#include <math.h>
#include <sys/wait.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma_language_dependent.h"
#include "tracy.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define FATALPREFIX						"(EMMA) "
#define EMMA_SUFFIX						".emma"
#define SOUNDDIR						"/sounds/"
#define TAGDESCR						"@@@"

//-- Maximal sizes
#define MSG_MAX_SIZE					512			// IPC message
#define KEY_MAX_SIZE					16			// Key (part of IPC message)
#define DIRECTION_MAX_SIZE				16			// Direction parameter
#define COLOR_MAX_SIZE					16			// Color parameter
#define ACTION_MAX_SIZE					32			// Action parameter
#define INST_MAX_SIZE					256			// Instruction as a whole
#define DESCR_MAX_SIZE					32
#define FILENAME_MAX_SIZE				20
#define EXPRESSION_MAX_SIZE				64			// Max expression size
#define VARIABLE_MAX_SIZE				8			// Max variable size
#define VARIABLE_MAX_NUMBER				50			// Max number of variables in a programm.
// To migrate to dynamic allocation later!!!

#define PROG_ROW						6
#define START_INSTRUCTION				7

#define CURSOR 							"🢂"
#define EXECCURSOR 						"⬤"
#define NOCURSOR 						"  "
// Sub-process names
#define PROGRAM							"emma_program"
#define BOARD							"emma_board"
// Request and response identificators
#define BOARD_REQUEST					0x00012345
#define BOARD_RESPONSE					0x00012346

// Key for messages
// The messages exchanged between processes have the following format:
//	KEY[:INFORMATION]
//		KEY: one of the values described below
//		INFORMATION: useful information for the requested processing
#define BOARD_MSG_START					"START"				// no additional information
#define BOARD_MSG_STOP					"STOP"				// no additional information
#define BOARD_MSG_END					"END"				// no additional information
#define BOARD_MSG_RESTART				"RESTART"			// no additional information
#define BOARD_MSG_HOME					"HOME"				// no additional information
#define BOARD_MSG_TURNRIGHT				"TURNRIGHT"			// no additional information
#define BOARD_MSG_TURNLEFT				"TURNLEFT"			// no additional information
#define BOARD_MSG_FORWARD				"FORWARD"			// number of steps
#define BOARD_MSG_BACKWARD				"BACKWARD"			// number of steps
#define BOARD_MSG_SKIP					"SKIP"				// number of steps
#define BOARD_MSG_DO					"DO"				// number of times
#define BOARD_MSG_COLOR					"COLOR"				// color
#define BOARD_MSG_ENDDO					"ENDDO"				// no additional information
#define BOARD_MSG_ASSIGN				"ASSIGN"			// Local, not sent to board
#define BOARD_UNKNOWN					"UNKNOWN"			// no additional information

#define ACKNOWLEDGMENT					"ACKNOWLEDGMENT"
#define WARNING_CRASH					"CRASH"

#define ERROR_CPPOTCE					"CPPOTCE"			// ERROR_CANNOT_GET_PATH_OF_THE_CURRENT_EXECUTABLE
#define ERROR_SDDNE						"SDDNE"				// ERROR_SOUNDS_DIRECTORY_DOES_NOT_EXIST
//-- Escaped chars
#define ESC_ATTRIBUTSOFF				"\033[0m"
#define ESC_STRONG_ON					"\033[1m"
#define ESC_EMPHASIS_ON					"\033[3m"
#define ESC_REVERSE_ON					"\033[7m"
#define ESC_STRIKE_ON					"\033[9m"
#define ESC_COLOR						"\033[%dm"
//-- Color management
#define FOREGROUND(color) 				(30 + color)
#define BACKGROUND(color) 				(40 + color)
//-- Macro code: Display
#define SAVEINITSCREEN(n)				do { \
											tputs(screeninit, (int)n, putchar); \
										} while (0)
#define RESTOREINITSCREEN(n)			do { \
											tputs(screendeinit, (int)n, putchar); \
										} while (0)
#define CLEAR_ENTIRE_SCREEN()			tputs(clear, (int)nbLINES, putchar)
#define MOVE_CURSOR(line, column)		tputs(tgoto(move, ((int)(column - 1) % nbCOLS), (int)(line - 1)), 1, putchar)
#define CLEAR_SCREEN_FROM_CURSOR_DOWN()	fprintf(stdout, "\033[J");
#define CLEAR_ENTIRE_LINE()				fprintf(stdout, "\033[2K");
#define HIDE_CURSOR()					fprintf(stdout, "\033[?25l")
#define SHOW_CURSOR()					fprintf(stdout, "\033[?25h")
#define DISPLAY_BELL()					do { \
											fprintf(stdout, "\033[?5h"); fflush(stdout); \
											usleep(50000); \
											fprintf(stdout, "\033[?5l"); fflush(stdout); \
										} while (0)
//-- SYSTEM V IPC Messages
#define CREATEIPC(from, to, msqid, key) _TRACY_PRINT("Create IPC from %s to %s with key 0x%08x", from, to, key); \
										if ((msqid = msgget(key, S_IRWXU | IPC_CREAT)) == -1) \
											emma_fatal(#from ": Cannot create new message queue for " #to " (errno='%s')", strerror(errno)); \
										_TRACY_PRINT("Create IPC: %d", msqid);

#define	WRITEIPC(to, id)				_TRACY_PRINT("Write IPC to %s with key %d: %s", to, id, msg.mtext); \
										do { \
											int _n = msgsnd(id, &msg, strlen(msg.mtext), IPC_PRIVATE); \
											_TRACY_PRINT("Write IPC: %d", _n); \
											if (_n == -1) emma_fatal("Cannot send message to " #to " (errno='%s')", strerror(errno)); \
										} while (0)

#define READIPC(to, id) 				_TRACY_PRINT("Read IPC from %s with key %d", to, id); \
										do { \
											ssize_t _n; \
											while (((_n = msgrcv(id, &msg, MSG_MAX_SIZE, IPCKEY, IPC_PRIVATE)) == -1) && errno == 4); \
											if (_n == -1) emma_fatal("Cannot receive message from " #to " (errno='%s')", strerror(errno)); \
											msg.mtext[_n] = '\0'; \
											_TRACY_PRINT("Received: %ld - <%s>", _n, msg.mtext); \
										} while (0)

#define DELETEIPC(msqid)				msgctl(msqid, IPC_RMID, IPC_PRIVATE);
#define SEND_BACK(status)				strncpy(msg.mtext, status, MSG_MAX_SIZE); \
										WRITEIPC(PROGRAM, board_response);
#define SEND_COMMAND1(cmd)				snprintf(msg.mtext, MSG_MAX_SIZE, "%s", cmd); \
										WRITEIPC(BOARD, board_request); \
										READIPC(BOARD, board_response);
#define SEND_COMMAND2(cmd, param)		snprintf(msg.mtext, MSG_MAX_SIZE, "%s:%d", cmd, param); \
										WRITEIPC(BOARD, board_request); \
										READIPC(BOARD, board_response);

#define MAX(a,b)						(a > b ? a : b)
#define MIN(a,b)						(a > b ? b : a)
//------------------------------------------------------------------------------
// TYPEDEFS
//------------------------------------------------------------------------------
typedef enum { FALSE = 0, TRUE = 1 } boolean;
typedef enum { black = 0, red, green, yellow, blue, magenta, cyan, white } e_color;
typedef enum { MODE_ECHOED = 0, MODE_RAW } e_terminal;
typedef enum { HORIZONTAL_RIGHT = 0, HORIZONTAL_LEFT, VERTICAL_UP, VERTICAL_DOWN } e_direction;
//------------------------------------------------------------------------------
// STRUCTURE DEFINITION
//------------------------------------------------------------------------------
// IPC message structure
typedef struct {
	long	mtype;
	char	mtext[MSG_MAX_SIZE + 1];
} s_msg;

typedef struct {
	char	instruction[INST_MAX_SIZE + 1];
	char	action[ACTION_MAX_SIZE + 1];
	char	parameter[EXPRESSION_MAX_SIZE + 1];
	char 	variable[VARIABLE_MAX_SIZE + 1];
	int 	counter_initial;
	int 	counter;
	boolean accuracy;
} s_instruction;

typedef struct {
	char name[VARIABLE_MAX_SIZE + 1];
	int value;
} s_variable;
//------------------------------------------------------------------------------
// FUNCTIONS
//------------------------------------------------------------------------------
//-- emma_main.c / emma_board.c
extern void		emma_cleanContext( void );
//-- emma_main.c
extern void		emma_clear_program( s_instruction *, int );
extern void		emma_refresh( void );
//-- emma_display.c
extern void		emma_display_message( const char * );
extern void		emma_display_error( const char * );
extern void		emma_display_indentation( FILE *, int );
extern void		emma_clear_instruction( int );
extern int 		emma_display_program( void );
extern void		emma_display_program_status( void );
//-- emma_funcs.c
extern void		emma_initScreen( void );
extern void		emma_endScreen( void );
extern void		emma_splitMsg(char *, char *, int *);
extern boolean	emma_existDir(char *);
extern void		emma_getTerminalSize( int *, int * );
extern void		emma_fatal( const char *, ... );
//-- emma_prg_executor.c
extern void		emma_executor( void );
//-- emma_prg_editor.c
extern int 		emma_editor( void );
//-- emma_prg_getchar.c
extern int 		emma_getchar( int * );
//-- emma_prg_instructions.c
extern boolean	emma_get_instruction( int, int );
extern void		emma_read_instructions( char *, int );
//-- emma_prg_file.c
extern void		emma_save( void );
extern void		emma_load( void );
//-- emma_prg_confirmation.c
extern boolean	emma_getConfirmationTheProgramNeedsToBeSaved();
//-- emma_prg_expression.c
extern boolean	emma_read_expression( char * );
extern char 	*emma_get_expression_from_string( char ** );
extern boolean	emma_check_expression( char * );
extern boolean	emma_evaluate_expression( char *, int *);
//-- emma_prg_variables.c
extern void		emma_clean_symbols_table( void );
extern boolean	emma_read_variable_name( char * );
extern char 	*emma_get_variable_name_from_string( char ** );
extern boolean	emma_get_variable_value( char *, int * );
extern void		emma_put_variable_value( char *, int );
extern void		emma_create_variable_if_needed( char * );
//------------------------------------------------------------------------------
#endif	// EMMA_H
