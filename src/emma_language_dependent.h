//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------
#ifndef EMMA_LANGUAGE_DEPENDENT_H
#define EMMA_LANGUAGE_DEPENDENT_H
//------------------------------------------------------------------------------
//-- Index of messages and errors
#define I_WAITING_PROGRAM						0
#define I_EXECUTION_ONGOING						1
#define I_EXECUTION_ENDED						2
#define I_EXEC_CRASH							3
#define I_EXEC_FATAL_1							4
#define I_EXEC_FATAL_2							5
#define I_EXEC_UNKNOWN							6
#define I_SYNTAX_ERROR							7
#define I_CANNOT_CREATE 						8
#define I_CANNOT_READ 							9
#define I_PROGRAM_WITH_ERROR					10
#define I_TOO_MANY_INSTRUCTIONS					11
#define I_NO_FILE_TO_LOAD						12
#define I_CANNOT_EVALUATE_EXPRESSION			13
//==============================================================================
#ifdef FRENCH
//--------------------------------------	----------------------------------------
#define DIFFERENTIAL_FOR_THE_INSTRUCTIONS_COLUMN	35
#define DIFFERENTIAL_FOR_THE_COMMANDS_COLUMN		35
#define DIFFERENTIAL_FOR_THE_STATUS_COLUMN			29

#define LGDEP_PROGRAM_TITLE						"PROGRAMMATION"
#define LGDEP_BOARD_TITLE						"EXECUTION"

#define LGDEP_WAITING_PROGRAM					"J'attends tes instructions..."
#define LGDEP_EXECUTION_ONGOING					"Exécution de tes instructions en cours..."
#define LGDEP_EXECUTION_ENDED					"Exécution terminée."
#define LGDEP_EXEC_CRASH						"!!! Tu as fais une erreur... et tu t'es payé(e) un mur !!!"
#define LGDEP_EXEC_FATAL_1						"ERREUR APPLICATION: Impossible d'obtenir le chemin de l'exécutable."
#define LGDEP_EXEC_FATAL_2						"ERREUR APPLICATION: Le fichier de répertoire des sons n'existe pas."
#define LGDEP_EXEC_UNKNOWN						"ERREUR APPLICATION: Erreur non référencée !!!"
#define LGDEP_SYNTAX_ERROR						"Il y a une ou plusieurs erreurs dans ton programme....."
#define LGDEP_CANNOT_CREATE 					"!!! Le fichier %s ne peut pas etre cree !!!"
#define LGDEP_CANNOT_READ 						"!!! Le fichier %s ne peut pas etre lu !!!"
#define LGDEP_PROGRAM_WITH_ERROR				"!!! Le programme comporte une ou plusieurs erreurs. Exécution impossible !!!"
#define LGDEP_TOO_MANY_INSTRUCTIONS				"!!! Le programme comporte trop d'instructions (plus que les %d autorisées) !!!"
#define LGDEP_NO_FILE_TO_LOAD					"!!! Il n'y a pas de programme à utiliser !!!"
#define LGDEP_CANNOT_EVALUATE_EXPRESSION		"!!! Erreur dans l'expression arithmétique !!!"

#define LGDEP_WELCOME							"│ BONJOUR et BIENVENUE"
#define LGDEP_PROGRAM_ELEMENTS()				\
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "          INSTRUCTIONS         " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ AVANCER expression PAS          │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ TOURNER GAUCHE                  │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ TOURNER DROITE                  │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ RECULER expression PAS          │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ SAUTER expression PAS           │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ COLORER EN couleur              │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ FAIRE expression FOIS ... FIN   │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ METTRE expression DANS variable │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰────────────────┬────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "    EXPRESSION (simplifiée)    " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> opérande + opérande         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> opérande - opérande         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> opérande × opérande         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> opérande / opérande         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ opérande --> variable ou nombre │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ variable --> suite de lettres   │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰────────────────┬────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "           COULEURS            " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ BLANC / BLEU / CYAN / JAUNE     │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ MAGENTA / NOIR / ROUGE / VERT   │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰────────────────┬────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "          COMMANDES            " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ EXECUTER     -->   Touche F2    │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ NETTOYER     -->   Touche F4    │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ ENREGISTRER  -->   Touche F6    │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ REUTILISER   -->   Touche F7    │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ QUITTER      -->   Touche F9    │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰─────────────────────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
#define LGDEP_YOURPROGRAM						"TON PROGRAMME:"
#define LGDEP_STATUS_MODIFIED_PROGRAM			" PROGRAMME MODIFIE "
#define LGDEP_STATUS_TO_BE_SAVED				"     A SAUVEGARDER "
#define LGDEP_STATUS_PROGRAM_SAVED				" PROGRAMME SAUVEGARDE "
#define LGDEP_STATUS_CLEAR						"                      "
#define LGDEP_ERRONEOUS_INSTRUCTION				"⯇ ERREUR"

#define LGDEP_KEYWORD_GOFORWARD					"AVANCER"
#define LGDEP_KEYWORD_GOBACKWARD				"RECULER"
#define LGDEP_KEYWORD_SKIP						"SAUTER"
#define LGDEP_KEYWORD_TURN						"TOURNER"
#define LGDEP_KEYWORD_DO 						"FAIRE"
#define LGDEP_KEYWORD_ENDDO 					"FIN"
#define LGDEP_KEYWORD_ASSIGN 					"METTRE"
#define LGDEP_KEYWORD_COLOR 					"COLORER"
#define LGDEP_KEYWORD_STEPS						"PAS"
#define LGDEP_KEYWORD_TIMES						"FOIS"
#define LGDEP_KEYWORD_LEFT						"GAUCHE"
#define LGDEP_KEYWORD_RIGHT						"DROITE"
#define LGDEP_KEYWORD_TO 						"DANS"
#define LGDEP_KEYWORD_IN 						"EN"
#define LGDEP_KEYWORD_BLACK						"NOIR"
#define LGDEP_KEYWORD_RED						"ROUGE"
#define LGDEP_KEYWORD_GREEN						"VERT"
#define LGDEP_KEYWORD_YELLOW					"JAUNE"
#define LGDEP_KEYWORD_BLUE						"BLEU"
#define LGDEP_KEYWORD_MAGENTA					"MAGENTA"
#define LGDEP_KEYWORD_CYAN						"CYAN"
#define LGDEP_KEYWORD_WHITE						"BLANC"

#define LGDEP_INSTRUCTION_GOFORWARD				"AVANCER %s PAS"
#define LGDEP_INSTRUCTION_GOBACKWARD			"RECULER %s PAS"
#define LGDEP_INSTRUCTION_SKIP					"SAUTER %s PAS"
#define LGDEP_INSTRUCTION_TURNLEFT				"TOURNER GAUCHE"
#define LGDEP_INSTRUCTION_TURNRIGHT				"TOURNER DROITE"
#define LGDEP_INSTRUCTION_DO 					"FAIRE %s FOIS"
#define LGDEP_INSTRUCTION_ENDDO					"FIN"
#define LGDEP_INSTRUCTION_ASSIGN				"METTRE %s DANS %s"
#define LGDEP_INSTRUCTION_COLOR					"COLORER EN %s"

#define LGDEP_SAVE_LINE1						"╭────────────────────────────────────────────────────────────────────╮"
#define LGDEP_SAVE_LINE2						"│ Répertoire courant : %45s │"
#define LGDEP_SAVE_LINE31						"│                                                                    │"
#define LGDEP_SAVE_LINE32						"│                      %45s │"
#define LGDEP_SAVE_LINE4						"│ Nom du fichier ? _________________________________________________ │"
#define LGDEP_SAVE_LINE5						"│ Description ?    _________________________________________________ │"
#define LGDEP_SAVE_LINE6						"╰────────────────────────────────────────────────────────────────────╯"

#define LGDEP_LOAD_LINE1						"╭────────────────────────────────────────────────────────────────╮"
#define LGDEP_LOAD_LINE2						"│ Choisis un programme :                                         │"
#define LGDEP_LOAD_LINE3						"│  ╭──────────────────────────────────────────────────────────╮  │"
#define LGDEP_LOAD_LINE4						"│  │ %-20s  %-32s ▒ │  │"
#define LGDEP_LOAD_LINE5						"│  ╰──────────────────────────────────────────────────────────╯  │"
#define LGDEP_LOAD_LINE6						"╰────────────────────────────────────────────────────────────────╯"

#define LGDEP_CONFIRM_LINE1						"╭───────────────────────────────────────────────────────╮"
#define LGDEP_CONFIRM_LINE2						"│ Le programme a été modifié. Souhaites-tu en faire une │"
#define LGDEP_CONFIRM_LINE3						"│ sauvegarde ?                                          │"
#define LGDEP_CONFIRM_LINE4						"│                                                       │"
#define LGDEP_CONFIRM_LINE50					"│       "
#define LGDEP_CONFIRM_LINE51					"   NON   "
#define LGDEP_CONFIRM_LINE52					"                        "
#define LGDEP_CONFIRM_LINE53					"   OUI   "
#define LGDEP_CONFIRM_LINE54					"      │"
#define LGDEP_CONFIRM_LINE6						"│                                                       │"
#define LGDEP_CONFIRM_LINE7						"╰───────────────────────────────────────────────────────╯"

#define LGDEP_ERROR_LINE1						"╭──────────────────────────────────────────────────────────────╮"
#define LGDEP_ERROR_LINE2						"│                   ERREUR DANS L'APPLICATION                  │"
#define LGDEP_ERROR_LINE3						"├──────────────────────────────────────────────────────────────┤"
#define LGDEP_ERROR_LINE4						"│                                                              │"
#define LGDEP_ERROR_LINE5						"│ %-60s │"
#define LGDEP_ERROR_LINE6						"│                                                              │"
#define LGDEP_ERROR_LINE7						"│ Navré ! ceci est une erreur d'EMMA. Merci de nous tenir      │"
#define LGDEP_ERROR_LINE8						"│ informés. Une fois que tu auras appuyé sur une touche,       │"
#define LGDEP_ERROR_LINE9						"│ le jeu se terminera.                                         │"
#define LGDEP_ERROR_LINE10						"╰──────────────────────────────────────────────────────────────╯"
//------------------------------------------------------------------------------
#endif	// FRENCH
//==============================================================================
#ifdef ENGLISH
//--------------------------------------	----------------------------------------
#define DIFFERENTIAL_FOR_THE_INSTRUCTIONS_COLUMN	35
#define DIFFERENTIAL_FOR_THE_COMMANDS_COLUMN		35
#define DIFFERENTIAL_FOR_THE_STATUS_COLUMN			29

#define LGDEP_PROGRAM_TITLE						"PROGRAMING"
#define LGDEP_BOARD_TITLE						"EXECUTION"

#define LGDEP_WAITING_PROGRAM					"I'm waiting for your instructions..."
#define LGDEP_EXECUTION_ONGOING					"Running your instructions in progress..."
#define LGDEP_EXECUTION_ENDED					"Execution completed."
#define LGDEP_EXEC_CRASH						"!!! You made a mistake ... and you hit the wall !!!"
#define LGDEP_EXEC_FATAL_1						"APPLICATION ERROR: Unable to get the path of the executable."
#define LGDEP_EXEC_FATAL_2						"APPLICATION ERROR: The sounds directory does not exist."
#define LGDEP_EXEC_UNKNOWN						"APPLICATION ERROR: Unreferenced error!!!"
#define LGDEP_SYNTAX_ERROR						"There are one or more errors in your program....."
#define LGDEP_CANNOT_CREATE 					"!!! File %s can not be created !!!"
#define LGDEP_CANNOT_READ 						"!!! File %s can not be read  !!!"
#define LGDEP_PROGRAM_WITH_ERROR				"!!! The program has one or more errors. Cannot run it !!!"
#define LGDEP_TOO_MANY_INSTRUCTIONS				"!!! The program has too many instructions (more than the %d allowed) !!!"
#define LGDEP_NO_FILE_TO_LOAD					"!!! There is no program to load !!!"
#define LGDEP_CANNOT_EVALUATE_EXPRESSION		"!!! Error in the arithmetic expression !!!"

#define LGDEP_WELCOME							"│ HELLO and WELCOME"
#define LGDEP_PROGRAM_ELEMENTS()				\
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "          INSTRUCTIONS         " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ MOVE FORWARD expression STEPS   │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ TURN LEFT                       │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ TURN RIGHT                      │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ GO BACK expression STEPS        │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ SKIP expression STEPS           │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ COLOR IN color                  │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ DO expression TIMES ... END     │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ ASSIGN expression TO variable   │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰────────────────┬────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "    EXPRESSION (simplified)    " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> operand + operand           │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> operand - operand           │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> operand × operand           │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ --> operand / operand           │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ operand --> variable or number  │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ variable --> a series of letters│" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰────────────────┬────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "            COLORS             " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ BLACK / BLUE / CYAN / GREEN     │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ MAGENTA / RED /  WHITE / YELLOW │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰────────────────┬────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╭────────────────┴────────────────╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ " ESC_REVERSE_ON "           COMMANDS            " ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow), FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ RUN       -->    Key F2         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ CLEAN     -->    Key F4         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ SAVE      -->    Key F6         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ LOAD      -->    Key F7         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│ EXIT      -->    Key F9         │" ESC_ATTRIBUTSOFF, FOREGROUND(yellow)); \
	MOVE_CURSOR(row++, column_explanation); fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰─────────────────────────────────╯" ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
#define LGDEP_YOURPROGRAM						"YOUR PROGRAM:"
#define LGDEP_STATUS_MODIFIED_PROGRAM			" MODIFIED PROGRAM "
#define LGDEP_STATUS_TO_BE_SAVED				"      TO BE SAVED "
#define LGDEP_STATUS_PROGRAM_SAVED				" PROGRAM SAVED "
#define LGDEP_STATUS_CLEAR						"                  "
#define LGDEP_ERRONEOUS_INSTRUCTION				"⯇ ERROR"

#define LGDEP_KEYWORD_GOFORWARD					"MOVE FORWARD"
#define LGDEP_KEYWORD_GOBACKWARD				"GO BACK"
#define LGDEP_KEYWORD_SKIP						"SKIP"
#define LGDEP_KEYWORD_TURN						"TURN"
#define LGDEP_KEYWORD_DO 						"DO"
#define LGDEP_KEYWORD_ENDDO 					"END"
#define LGDEP_KEYWORD_ASSIGN 					"ASSIGN"
#define LGDEP_KEYWORD_COLOR 					"COLOR"
#define LGDEP_KEYWORD_STEPS						"STEPS"
#define LGDEP_KEYWORD_TIMES						"TIMES"
#define LGDEP_KEYWORD_LEFT						"LEFT"
#define LGDEP_KEYWORD_RIGHT						"RIGHT"
#define LGDEP_KEYWORD_TO 						"TO"
#define LGDEP_KEYWORD_IN 						"IN"
#define LGDEP_KEYWORD_BLACK						"BLACK"
#define LGDEP_KEYWORD_RED						"RED"
#define LGDEP_KEYWORD_GREEN						"GREEN"
#define LGDEP_KEYWORD_YELLOW					"YELLOW"
#define LGDEP_KEYWORD_BLUE						"BLUE"
#define LGDEP_KEYWORD_MAGENTA					"MAGENTA"
#define LGDEP_KEYWORD_CYAN						"CYAN"
#define LGDEP_KEYWORD_WHITE						"WHITE"

#define LGDEP_INSTRUCTION_GOFORWARD				"MOVE FORWARD %s STEPS"
#define LGDEP_INSTRUCTION_GOBACKWARD			"GO BACK %s STEPS"
#define LGDEP_INSTRUCTION_SKIP					"SKIP %s STEPS"
#define LGDEP_INSTRUCTION_TURNLEFT				"TURN LEFT"
#define LGDEP_INSTRUCTION_TURNRIGHT				"TURN RIGHT"
#define LGDEP_INSTRUCTION_DO 					"DO %s TIMES"
#define LGDEP_INSTRUCTION_ENDDO					"END"
#define LGDEP_INSTRUCTION_ASSIGN				"ASSIGN %s TO %s"
#define LGDEP_INSTRUCTION_COLOR					"COLOR IN %s"

#define LGDEP_SAVE_LINE1						"╭────────────────────────────────────────────────────────────────╮"
#define LGDEP_SAVE_LINE2						"│ Current directory: %47s │"
#define LGDEP_SAVE_LINE31						"│                                                                │"
#define LGDEP_SAVE_LINE32						"│                    %47s │"
#define LGDEP_SAVE_LINE4						"│ File name?   _________________________________________________ │"
#define LGDEP_SAVE_LINE5						"│ Description? _________________________________________________ │"
#define LGDEP_SAVE_LINE6						"╰────────────────────────────────────────────────────────────────╯"

#define LGDEP_LOAD_LINE1						"╭────────────────────────────────────────────────────────────────╮"
#define LGDEP_LOAD_LINE2						"│ Choose a program:                                              │"
#define LGDEP_LOAD_LINE3						"│  ╭──────────────────────────────────────────────────────────╮  │"
#define LGDEP_LOAD_LINE4						"│  │ %-20s  %-32s ▒ │  │"
#define LGDEP_LOAD_LINE5						"│  ╰──────────────────────────────────────────────────────────╯  │"
#define LGDEP_LOAD_LINE6						"╰────────────────────────────────────────────────────────────────╯"

#define LGDEP_CONFIRM_LINE1						"╭───────────────────────────────────────────────────────╮"
#define LGDEP_CONFIRM_LINE2						"│ The program has been modified. Do you want to make a  │"
#define LGDEP_CONFIRM_LINE3						"│ backup ?                                              │"
#define LGDEP_CONFIRM_LINE4						"│                                                       │"
#define LGDEP_CONFIRM_LINE50					"│       "
#define LGDEP_CONFIRM_LINE51					"   NO    "
#define LGDEP_CONFIRM_LINE52					"                        "
#define LGDEP_CONFIRM_LINE53					"   YES   "
#define LGDEP_CONFIRM_LINE54					"      │"
#define LGDEP_CONFIRM_LINE6						"│                                                       │"
#define LGDEP_CONFIRM_LINE7						"╰───────────────────────────────────────────────────────╯"

#define LGDEP_ERROR_LINE1						"╭──────────────────────────────────────────────────────────────╮"
#define LGDEP_ERROR_LINE2						"│                       APPLICATION ERROR                      │"
#define LGDEP_ERROR_LINE3						"├──────────────────────────────────────────────────────────────┤"
#define LGDEP_ERROR_LINE4						"│                                                              │"
#define LGDEP_ERROR_LINE5						"│ %-60s │"
#define LGDEP_ERROR_LINE6						"│                                                              │"
#define LGDEP_ERROR_LINE7						"│ Sorry! this is an EMMA error. Thank you for keeping us       │"
#define LGDEP_ERROR_LINE8						"│ informed. Once you press any key, the game will end.         │"
#define LGDEP_ERROR_LINE9						"│                                                              │"
#define LGDEP_ERROR_LINE10						"╰──────────────────────────────────────────────────────────────╯"
//------------------------------------------------------------------------------
#endif	// ENGLISH
//==============================================================================
#endif	// EMMA_LANGUAGE_DEPENDENT_H
