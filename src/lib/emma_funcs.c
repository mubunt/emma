//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern int 		nbLINES;			// Numbero f rows of the terminal
extern int 		nbCOLS;				// Numbero f cols of the terminal
extern char 	*screeninit;		// Startup terminal initialization
extern char 	*screendeinit;		// Exit terminal de-initialization
extern char 	*clear;				// Clear screen
extern char 	*move;				// Cursor positioning
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static __sighandler_t 				initial_signal;
//------------------------------------------------------------------------------
// LOCAL ROUTINES
//------------------------------------------------------------------------------
static void setTerminalMode( e_terminal mode ) {
	static struct termios cooked, raw;
	switch (mode) {
	case MODE_ECHOED:
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &cooked)) {
			emma_fatal("%s", "Cannot set the parameter 'MODE_ECHOED' associated with the terminal. Abort!");
		}
		break;
	case MODE_RAW:
		if (0 != tcgetattr(STDIN_FILENO, &cooked)) {
			emma_fatal("%s", "Cannot set the parameter 'MODE_RAW' associated with the terminal. Abort!");
		}
		raw = cooked;
		cfmakeraw(&raw);
		if (0 != tcsetattr(STDIN_FILENO, TCSANOW, &raw)) {
			emma_fatal("%s", "Cannot set the parameter 'MODE_RAW/TCSANOW' associated with the terminal. Abort!");
		}
		break;
	}
}
//------------------------------------------------------------------------------
static void getTerminalCapabilities( void ) {
#define	TERMBUF_SIZE		2048
#define	TERMSBUF_SIZE		1024
#define	DEFAULT_TERM		"unknown"
	// Find out what kind of terminal this is.
	char *term;
	char termbuf[TERMBUF_SIZE];
	if ((term = getenv("TERM")) == NULL) term = (char *)DEFAULT_TERM;
	// Load information relative to the terminal from the erminfo database.
	switch (tgetent(termbuf, term)) {
	case -1:
		emma_fatal("%s", "The terminfo database could not be found. Abort!");
		break;
	case 0:
		emma_fatal("%s", "There is no entry for this terminal in the terminfo database. Abort!");
		break;
	default:
		break;
	}
	// Get various string-valued capabilities.
	if (NULL == (screeninit = tgetstr("ti", NULL)))
		emma_fatal("%s", "There is no entry 'ti' (screen init) for this terminal in the terminfo database. Abort!");
	if (NULL == (screendeinit = tgetstr("te", NULL)))
		emma_fatal("%s", "There is no entry 'te' (screen deinit) for this terminal in the terminfo database. Abort!");
	if (NULL == (clear = tgetstr("cl",NULL)))
		emma_fatal("%s", "There is no entry 'cl' (clear screen) for this terminal in the terminfo database. Abort!");
	if (NULL == (move = tgetstr("cm", NULL)))
		emma_fatal("%s", "There is no entry 'cm' (cursor move) for this terminal in the terminfo database. Abort!");
}

//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
void emma_getTerminalSize( int *lines, int *cols ) {
	struct winsize w;
	if (0 != ioctl(STDOUT_FILENO, TIOCGWINSZ, &w))
		emma_fatal("%s", "Cannot get terminal size Abort!");
	*lines = w.ws_row;
	*cols = w.ws_col;
}
//------------------------------------------------------------------------------
void emma_initScreen( void ) {
	initial_signal = signal(SIGWINCH, SIG_IGN);
	setTerminalMode(MODE_RAW);
	getTerminalCapabilities();
//	Next two line commented since Ubuntu 18.10. Refer to README file
//	emma_getTerminalSize(&nbLINES, &nbCOLS);
	SAVEINITSCREEN(nbLINES);
	fflush(stdout);
}
//------------------------------------------------------------------------------
void emma_endScreen( void ) {
	setTerminalMode(MODE_ECHOED);
	RESTOREINITSCREEN(nbLINES);
	fflush(stdout);
	signal(SIGWINCH, initial_signal);
}
//------------------------------------------------------------------------------
void emma_splitMsg(char *msg, char *key, int *steps) {
#define KEYSEP		":"
	char tmp[MSG_MAX_SIZE + 1];
	char info[MSG_MAX_SIZE + 1];
	strncpy(tmp, msg, MSG_MAX_SIZE);
	(void) strtok(tmp, KEYSEP);
	if (msg[strlen(tmp)] == KEYSEP[0]) {
		strncpy(key, tmp, KEY_MAX_SIZE);
		strncpy(info, tmp + strlen(key) + 1, MSG_MAX_SIZE);
		*steps = (int) strtol(info, (char **)NULL, 10);
	} else {
		//*key = '\0';
		//strncpy(info, msg, MSG_MAX_SIZE);
		strncpy(key, tmp, KEY_MAX_SIZE);
		*steps = 0;
	}
}
//------------------------------------------------------------------------------
boolean emma_existDir(char *dir) {
	struct stat locstat;
	if (dir == NULL) return FALSE;
	if (stat(dir, &locstat) < 0) return FALSE;
	if (! S_ISDIR(locstat.st_mode)) return FALSE;
	return TRUE;
}
//------------------------------------------------------------------------------
#define MSG_MAX_LEN	60

void emma_fatal( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);

	int row = 20;
	int column = 10;
	HIDE_CURSOR();
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE1 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE2 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE3 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE4 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	if (strlen(buff) <= MSG_MAX_LEN) {
		MOVE_CURSOR(row++, column);
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE5 ESC_ATTRIBUTSOFF, FOREGROUND(red), buff);
	} else {
		char *pt = buff;
		do {
			size_t n = MIN(strlen(pt), MSG_MAX_LEN);
			char cc = pt[n];
			pt[n] = '\0';
			MOVE_CURSOR(row++, column);
			fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE5 ESC_ATTRIBUTSOFF, FOREGROUND(red), pt);
			pt[n] = cc;
			pt = pt + n;
		} while (*pt != '\0');
	}
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE6 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE7 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE8 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE9 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_ERROR_LINE10 ESC_ATTRIBUTSOFF, FOREGROUND(red));
	fflush(stdout);

	int c = getchar();
	emma_cleanContext();
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
