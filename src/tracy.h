//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
// tracy: Simple C trace macros with output to a text file.
// IMPORTANT: Header file modified for emma purpose.
//-------------------------------------------------------------------------------
#ifndef TRACY_H
#define TRACY_H
#define TRACY_VERSION				1.0.2

#ifdef TRACY
extern FILE 						*tracyfd;

#define TRACYFD_DEFINITION 			FILE *tracyfd = NULL;
#define _TRACYFD					tracyfd
#define _TRACYPROGRAM				"./tracy_program.log"
#define _TRACYBOARD					"./tracy_board.log"
#define _TRACYFORMAT				"%-16s | %04d | %-16s | "
#define _TRACY_START(log)				do { \
										_TRACYFD = fopen(log, "w"); \
										fprintf(_TRACYFD, _TRACYFORMAT "Beginning of Traces\n", __FILE__, __LINE__, __func__); \
									} while (0);
#define _TRACY_STOP()				do { \
										if (_TRACYFD != NULL) { \
											fprintf(_TRACYFD, _TRACYFORMAT "End of Traces\n", __FILE__, __LINE__, __func__); \
											fclose(_TRACYFD); \
										} \
									} while (0);
#define _TRACY_PRINT(format, ...)	do { \
										if (_TRACYFD != NULL) { \
											fprintf(_TRACYFD, _TRACYFORMAT format "\n", __FILE__, __LINE__, __func__,  __VA_ARGS__); \
											fflush(_TRACYFD); \
										} \
									} while (0);
#else
#define TRACYFD_DEFINITION
#define _TRACY_START(log)
#define _TRACY_STOP()
#define _TRACY_PRINT(format, ...)
#endif

#endif	// TRACY_H
//==============================================================================
