//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define EMMA_PACKAGE			"EMMA"
#define EMMA_PURPOSE			"Application to teach young children the basics of programming"
#define EMMA_COPYRIGHT			"Copyright (c) 2018, Michel RIZZO. All Rights Reserved."
#ifdef VERSION
#define EMMA_VERSION(x) 		str(x)
#define str(x)					#x
#else
#define EMMA_VERSION 			"Unknown"
#endif

#define ProcessPseudoFileSystem	"/proc/self/exe"
#define MAX_PARAMETER_LEN		PATH_MAX
#define MAX_OPTION_LEN			32

#define EXIST(x)				(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)				(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)

#define EMMA_DIRECTORY			"./emma-programs"
#define TERMINAL				"xfce4-terminal"

#define BOARD_GEOMETRY_ROWS		100
#define BOARD_GEOMETRY_COLS		50
#define BOARD_COORDINATES		"600+50"

#define PROGRAM_GEOMETRY_ROWS	80
#define PROGRAM_GEOMETRY_COLS	58
#define PROGRAM_COORDINATES		"20+50"
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	pid_t	procpid;
} watchdog;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static const char *emmaFiles[] = {
	"emma_board",
	"emma_program",
	"sounds/Car-crash-sound-effect.mp3",
	"sounds/Car-startup-fail.mp3",
	""
};
static pid_t pid_board;
static pid_t pid_program;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void fatal( const char *format, ... ) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "\n" ESC_COLOR ESC_STRONG_ON "%s%s" ESC_ATTRIBUTSOFF "\n\n", FOREGROUND(red), FATALPREFIX, buff);
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
static void usage(char *exec) {
	fprintf(stdout, "Usage: %s [ -V | -h ]\n", exec);
	fprintf(stdout, "	with:\n\
		-V: Display version and exit\n\
		-h: Display help and exit\n\n");
}
//------------------------------------------------------------------------------
// Kill all sub-processes already launched when one has failed
static void killSubProcesses( void ) {
	if (pid_board != -1) kill(pid_board, SIGTERM);
	if (pid_program != -1) kill(pid_board, SIGTERM);
}
//------------------------------------------------------------------------------
// Launch the process specified in argv[0] with optional options
static pid_t launch(char **argv) {
	pid_t  pid;
	if ((pid = fork()) < 0) {			// fork a child process
		killSubProcesses();
		fatal("%s", "Forking child process failed.");
	}
	if (pid == 0) {						// for the child process
		if (execvp(*argv, argv) < 0) {	// execute the command
			killSubProcesses();
			fatal("Exec failed %s (errno='%s')", *argv, strerror(errno));
		}
	}
	return(pid);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main(int argc, char *argv[]) {
	extern char *optarg;
	extern int optind, opterr;
	int errflg = 0, c;
	//--------------------------------------------------------------------------
	char *progname = argv[0];
	//---- Signals
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
	//--------------------------------------------------------------------------
	// Parameter checking and setting
	opterr = 0;
	while ((c = getopt (argc, argv, "hV")) != -1) {
		switch (c) {
		case 'h':
			usage(progname);
			return(EXIT_SUCCESS);
		case 'V':
			fprintf(stdout, "%s - %s\n", EMMA_PACKAGE, EMMA_COPYRIGHT);
			fprintf(stdout, "%s - %s\n", EMMA_PACKAGE, EMMA_PURPOSE);
			fprintf(stdout, "%s - Version %s\n\n", EMMA_PACKAGE, EMMA_VERSION(VERSION));
			return(EXIT_SUCCESS);
		case '?':
			errflg++;
			break;
		default:
			fatal("%s", "Abnormal situation during parameter checking. Abort!");
		}
	}
	if (errflg) {
		usage(progname);
		return(EXIT_FAILURE);
	}
	//--------------------------------------------------------------------------
	struct stat locstat, *ptlocstat;        // stat variables for EXIST*
	char execpath[PATH_MAX / 2 + 1];

	ptlocstat = &locstat;
	execpath[0] = '\0';
	//---- Get real path of this executable to compute the path JARS files
	// /proc/self is a symbolic link to the process-ID subdir
	// of /proc, e.g. /proc/4323 when the pid of the process
	// of this program is 4323.
	// Inside /proc/<pid> there is a symbolic link to the
	// executable that is running as this <pid>.  This symbolic
	// link is called "exe".
	// So if we read the path where the symlink /proc/self/exe
	// points to we have the full path of the executable.
	if (readlink(ProcessPseudoFileSystem, execpath, sizeof(execpath)) == -1)
		fatal("%s", "Cannot get path of the current executable.");
	dirname(execpath);
	//---- Check consistency of the installation
	char emmafile[PATH_MAX];
	snprintf(emmafile, sizeof(emmafile), "%s%s", execpath, SOUNDDIR);
	if (! EXISTDIR(emmafile))
		fatal("WRONG INSTALLATION - Directory '%s' does not exist.", emmafile);
	int i = 0;
	while (strlen(emmaFiles[i]) != 0) {
		snprintf(emmafile, sizeof(emmafile), "%s/%s", execpath, emmaFiles[i]);
		if (! EXIST(emmafile))
			fatal("WRONG INSTALLATION - File '%s' does not exist.", emmafile);
		++i;
	}
	if (! EXISTDIR(EMMA_DIRECTORY))
		mkdir(EMMA_DIRECTORY, 0700);
	if (0 != chdir(EMMA_DIRECTORY))
		fatal("Cannot change directory to %s", EMMA_DIRECTORY);
	//---- Generation of "argv" parameter for "Board"
	char *arg1[9];
	char *cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	strcpy(cmd, TERMINAL);
	arg1[0] = cmd;
	cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	sprintf(cmd, "--geometry=%dx%d+%s", BOARD_GEOMETRY_ROWS, BOARD_GEOMETRY_COLS, BOARD_COORDINATES);
	arg1[1] = cmd;
	cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	sprintf(cmd, "--title=\"%s v%s - %s\"", EMMA_PACKAGE, EMMA_VERSION(VERSION), LGDEP_BOARD_TITLE);
	arg1[2] = cmd;
	cmd = malloc(MAX_OPTION_LEN * sizeof(char));
	strcpy(cmd, "--hide-toolbar");
	arg1[3] = cmd;
	cmd = malloc(MAX_OPTION_LEN * sizeof(char));
	strcpy(cmd, "--hide-menubar");
	arg1[4] = cmd;
	cmd = malloc(MAX_OPTION_LEN * sizeof(char));
	strcpy(cmd, "--hide-scrollbar");
	arg1[5] = cmd;
	cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	snprintf(cmd, MAX_PARAMETER_LEN, "--command=""%s/%s -r %d -c %d""", execpath, emmaFiles[0], BOARD_GEOMETRY_ROWS, BOARD_GEOMETRY_COLS);
	arg1[6] = cmd;
	arg1[7] = NULL;

	//---- Generation of "argv" parameter for "Program"
	char *arg2[9];
	cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	strcpy(cmd, TERMINAL);
	arg2[0] = cmd;
	cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	sprintf(cmd, "--geometry=%dx%d+%s", PROGRAM_GEOMETRY_ROWS, PROGRAM_GEOMETRY_COLS, PROGRAM_COORDINATES);
	arg2[1] = cmd;
	cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	sprintf(cmd, "--title=\"%s v%s - %s\"", EMMA_PACKAGE, EMMA_VERSION(VERSION), LGDEP_PROGRAM_TITLE);
	arg2[2] = cmd;
	cmd = malloc(MAX_OPTION_LEN * sizeof(char));
	strcpy(cmd, "--hide-toolbar");
	arg2[3] = cmd;
	cmd = malloc(MAX_OPTION_LEN * sizeof(char));
	strcpy(cmd, "--hide-menubar");
	arg2[4] = cmd;
	cmd = malloc(MAX_OPTION_LEN * sizeof(char));
	strcpy(cmd, "--hide-scrollbar");
	arg2[5] = cmd;
	cmd = malloc(MAX_PARAMETER_LEN * sizeof(char));
	snprintf(cmd, MAX_PARAMETER_LEN, "--command=""%s/%s -r %d -c %d""", execpath, emmaFiles[1], PROGRAM_GEOMETRY_ROWS, PROGRAM_GEOMETRY_COLS);
	arg2[6] = cmd;
	arg2[7] = NULL;

	//---- Running sub-processes
	pid_board = pid_program = -1;
	pid_board = launch(arg1);
	pid_program = launch(arg2);
	//---- Free
	c = 0;
	while (arg1[c] != NULL) free(arg1[c++]);
	c = 0;
	while (arg2[c] != NULL) free(arg2[c++]);
	//---- Signals
	signal(SIGABRT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGINT, SIG_DFL);
	//---- Exit
	return(EXIT_SUCCESS);
}
//------------------------------------------------------------------------------
