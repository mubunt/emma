//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
#include "emma_prg_getchar.h"
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
int emma_getchar( int *current_char ) {
	int c;
	int value = _unknown;
	switch ((*current_char = getchar())) {
	case DEL:
		value = _del;
		break;
	case BS:
		value = _bs;
		break;
	case CR:
		value = _cr;
		break;
	case TAB:
		value = _tab;
		break;
	case SPACE:
		value = _space;
		break;
	case ESCAPE:
		switch ((c = getchar())) {
		case O:
			c = getchar();
			switch (c) {
			case 'P':			// F1
				value = _f1;
				break;
			case 'Q':			// F2
				value = _f2;
				break;
			case 'R':			// F3
				value = _f3;
				break;
			case 'S':			// F4
				value = _f4;
				break;
			default:
				value = _unknown;
				break;
			}
			break;
		case OPENBRACKET:
			switch ((c = getchar())) {
			case '1':
				c = getchar();
				switch (c) {
				case '5':		// F5
					c = getchar();
					value = _f5;
					break;
				case '7':		// F6
					c = getchar();
					value = _f6;
					break;
				case '8':		// F7
					c = getchar();
					value = _f7;
					break;
				case '9':		// F8
					c = getchar();
					value = _f8;
					break;
				default:
					value = _unknown;
					break;
				}
				break;
			case '2':
				switch ((c = getchar())) {
				case '0':		// F9
					c = getchar();
					value = _f9;
					break;
				case '1':		// F10
					c = getchar();
					value = _f10;
					break;
				case '3':		// F11
					c = getchar();
					value = _f11;
					break;
				case '4':		// F12
					c = getchar();
					value = _f12;
					break;
				default:
					value = _unknown;
					break;
				}
				break;
			case '3':
				c = getchar();
				if (c == '~')	// Suppr
					value = _suppr;
				else
					value = _unknown;
				break;
			case ARROW_UP:		// Arrow Up
				value = _arrow_up;
				break;
			case ARROW_DOWN:	// Arrow Down
				value = _arrow_down;
				break;
			case ARROW_LEFT:	// Arrow Left
				value = _arrow_left;
				break;
			case ARROW_RIGHT:	// Arrow Right
				value = _arrow_right;
				break;
			default:
				value = _unknown;
				break;
			}
			break;
		default:
			value = _unknown;
			break;
		}
		break;
	default:
		value = _other;
		break;
	}
	return value;
}
//------------------------------------------------------------------------------
