//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
#include "emma_prg_getchar.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern int 					nbLINES;
extern int 					nbCOLS;
extern char 				*screeninit;	// Startup terminal initialization
extern char 				*screendeinit;	// Exit terminal de-initialization
extern char 				*clear;			// Clear screen
extern char 				*move;			// Cursor positioning
extern boolean				program_to_be_saved;
//------------------------------------------------------------------------------
// MACRO CODES
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int	row_button;
static char	nohighlighted[512];
static char	yeshighlighted[512];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void display_confirmation_panel( int row, int column ) {
	sprintf(nohighlighted, ESC_COLOR ESC_STRONG_ON
	        LGDEP_CONFIRM_LINE50
	        ESC_REVERSE_ON LGDEP_CONFIRM_LINE51 ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON
	        LGDEP_CONFIRM_LINE52
	        LGDEP_CONFIRM_LINE53
	        LGDEP_CONFIRM_LINE54, FOREGROUND(yellow), FOREGROUND(yellow));
	sprintf(yeshighlighted, ESC_COLOR ESC_STRONG_ON
	        LGDEP_CONFIRM_LINE50
	        LGDEP_CONFIRM_LINE51
	        LGDEP_CONFIRM_LINE52
	        ESC_REVERSE_ON LGDEP_CONFIRM_LINE53 ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON
	        LGDEP_CONFIRM_LINE54, FOREGROUND(yellow), FOREGROUND(yellow));

	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_CONFIRM_LINE1 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_CONFIRM_LINE2 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_CONFIRM_LINE3 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_CONFIRM_LINE4 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	row_button = row;
	MOVE_CURSOR(row++, column);
	fprintf(stdout, "%s", nohighlighted);
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_CONFIRM_LINE6 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_CONFIRM_LINE7 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	fflush(stdout);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
boolean emma_getConfirmationTheProgramNeedsToBeSaved( void ) {
	int row = 20;
	int column = 10;
	display_confirmation_panel(row, column);
	HIDE_CURSOR();
	boolean confirmation = FALSE;
	boolean stop = FALSE;
	int current_char;;
	while (1) {
		int c = emma_getchar(&current_char);
		switch (c) {
		case _cr:
			stop = TRUE;;
			break;
		case _arrow_left:
			if (confirmation) {
				confirmation = FALSE;
				MOVE_CURSOR(row_button, column);
				fprintf(stdout, "%s", nohighlighted);
				fflush(stdout);
			} else
				DISPLAY_BELL();
			break;
		case _arrow_right:
			if (confirmation)
				DISPLAY_BELL();
			else {
				confirmation = TRUE;
				MOVE_CURSOR(row_button, column);
				fprintf(stdout, "%s", yeshighlighted);
				fflush(stdout);
			}
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (stop) break;
	}
	emma_refresh();
	return confirmation;
}
//------------------------------------------------------------------------------
