//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------
#ifndef EMMA_GETCHAR_H
#define EMMA_GETCHAR_H
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define _unknown						-1
#define _del							0
#define _bs								1
#define _cr								2
#define _tab							3
#define _space							4
#define _f1 							5
#define _f2 							6
#define _f3 							7
#define _f4 							8
#define _f5 							9
#define _f6 							10
#define _f7 							11
#define _f8 							12
#define _f9 							13
#define _f10 							14
#define _f11 							15
#define _f12 							16
#define _suppr 							17
#define _arrow_up						18
#define _arrow_down						19
#define _arrow_left						20
#define _arrow_right					21
#define _other							22
//------------------------------------------------------------------------------
#define ESCAPE							0x1b
#define CR 								0x0d
#define TAB								0x09
#define BS 								0x08
#define DEL 							0x7f
#define SPACE							' '
#define O 								'O'
#define OPENBRACKET						'['
#define ARROW_UP						'A'
#define ARROW_DOWN						'B'
#define ARROW_RIGHT						'C'
#define ARROW_LEFT						'D'
//------------------------------------------------------------------------------
#endif	// EMMA_GETCHAR_H
