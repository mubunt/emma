//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
#include "emma_prg_getchar.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define RECORD_CORRECT_INSTRUCTION1(_action, _instruction, _expression, _variable) \
								strcpy(program[prgline].action, _action); \
								strcpy(program[prgline].parameter, _expression); \
								sprintf(program[prgline].instruction, _instruction, _expression); \
								strcpy(program[prgline].variable, _variable); \
								program[prgline].counter_initial = program[prgline].counter = -1; \
								program[prgline].accuracy = TRUE;
#define RECORD_CORRECT_INSTRUCTION2(_action, _instruction) \
								strcpy(program[prgline].action, _action); \
								program[prgline].counter_initial = program[prgline].counter = -1; \
								strcpy(program[prgline].parameter, ""); \
								strcpy(program[prgline].instruction, _instruction); \
								strcpy(program[prgline].variable, ""); \
								program[prgline].accuracy = TRUE;
#define RECORD_CORRECT_INSTRUCTION3(_action, _instruction, _expression, _variable) \
								strcpy(program[prgline].action, _action); \
								strcpy(program[prgline].parameter, _expression); \
								sprintf(program[prgline].instruction, _instruction, _expression, _variable); \
								strcpy(program[prgline].variable, _variable); \
								program[prgline].counter_initial = program[prgline].counter = -1; \
								program[prgline].accuracy = TRUE;
#define RECORD_WRONG_INSTRUCTION(_action, _instruction) \
								strcpy(program[prgline].action, _action); \
								strcpy(program[prgline].instruction, _instruction); \
								strcpy(program[prgline].parameter, ""); \
								strcpy(program[prgline].variable, ""); \
								program[prgline].counter_initial = program[prgline].counter = -1; \
								program[prgline].accuracy = FALSE;
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern s_instruction		*program;
extern const char			*messages[];
extern const char 			*colorLabel[];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static boolean get_direction( char *direction ) {
	*direction = '\0';
	int current_char;
	while (1) {
		int c = emma_getchar(&current_char);
		switch (c) {
		case _del:
		case _bs:
			return FALSE;
		case _other:
			current_char = toupper(current_char);
			if (current_char == LGDEP_KEYWORD_LEFT[0])
				strcpy(direction, LGDEP_KEYWORD_LEFT);
			else if (current_char == LGDEP_KEYWORD_RIGHT[0])
				strcpy(direction, LGDEP_KEYWORD_RIGHT);
			else
				DISPLAY_BELL();
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (*direction != '\0') break;
	}
	return TRUE;
}
//------------------------------------------------------------------------------
#define BLACK()		fprintf(stdout, "%s", LGDEP_KEYWORD_BLACK); strcpy(color, LGDEP_KEYWORD_BLACK);
#define RED()		fprintf(stdout, "%s", LGDEP_KEYWORD_RED); strcpy(color, LGDEP_KEYWORD_RED);
#define GREEN()		fprintf(stdout, "%s", LGDEP_KEYWORD_GREEN); strcpy(color, LGDEP_KEYWORD_GREEN);
#define YELLOW()	fprintf(stdout, "%s", LGDEP_KEYWORD_YELLOW); strcpy(color, LGDEP_KEYWORD_YELLOW);
#define BLUE()		fprintf(stdout, "%s", LGDEP_KEYWORD_BLUE); strcpy(color, LGDEP_KEYWORD_BLUE);
#define WHITE()		fprintf(stdout, "%s", LGDEP_KEYWORD_WHITE); strcpy(color, LGDEP_KEYWORD_WHITE);
#define CYAN()		fprintf(stdout, "%s", LGDEP_KEYWORD_CYAN); strcpy(color, LGDEP_KEYWORD_CYAN);
#define MAGENTA()	fprintf(stdout, "%s", LGDEP_KEYWORD_MAGENTA); strcpy(color, LGDEP_KEYWORD_MAGENTA);
//------------------------------------------------------------------------------
static boolean get_color( char *color ) {
	*color = '\0';
	int current_char;
	while (1) {
		int c = emma_getchar(&current_char);
		switch (c) {
		case _del:
		case _bs:
			return FALSE;
#ifdef FRENCH
		case _other:
			current_char = toupper(current_char);
			if (current_char == LGDEP_KEYWORD_BLACK[0]) {
				BLACK();
			} else if (current_char == LGDEP_KEYWORD_RED[0]) {
				RED();
			} else if (current_char == LGDEP_KEYWORD_GREEN[0]) {
				GREEN();
			} else if (current_char == LGDEP_KEYWORD_YELLOW[0]) {
				YELLOW();
			} else if (current_char == LGDEP_KEYWORD_MAGENTA[0]) {
				MAGENTA();
			} else if (current_char == LGDEP_KEYWORD_CYAN[0]) {
				CYAN();
			} else if (current_char == LGDEP_KEYWORD_BLUE[0]) {	// or LGDEP_KEYWORD_WHITE[0]
				fprintf(stdout, "%c", LGDEP_KEYWORD_BLUE[0]);
				int current_char;
				while (1) {
					c = emma_getchar(&current_char);
					switch (c) {
					case _del:
					case _bs:
						return FALSE;
					case _other:
						current_char = toupper(current_char);
						if (current_char == LGDEP_KEYWORD_BLUE[1]) {	// or LGDEP_KEYWORD_WHITE[1]
							fprintf(stdout, "%c", LGDEP_KEYWORD_BLUE[1]);
							while (1) {
								c = emma_getchar(&current_char);
								switch (c) {
								case _del:
								case _bs:
									return FALSE;
								case _other:
									current_char = toupper(current_char);
									if (current_char == LGDEP_KEYWORD_BLUE[2]) {
										fprintf(stdout, "%s", LGDEP_KEYWORD_BLUE + 2);
										strcpy(color, LGDEP_KEYWORD_BLUE);
									} else if (current_char == LGDEP_KEYWORD_WHITE[2]) {
										fprintf(stdout, "%s", LGDEP_KEYWORD_WHITE + 2);
										strcpy(color, LGDEP_KEYWORD_WHITE);
									} else
										DISPLAY_BELL();
									break;
								default:
									DISPLAY_BELL();
									break;
								}
								if (*color != '\0') break;
							}
						} else
							DISPLAY_BELL();
						break;
					default:
						DISPLAY_BELL();
						break;
					}
					if (*color != '\0') break;
				}
			} else
				DISPLAY_BELL();
			break;
#endif

#ifdef ENGLISH
		case _other:
			current_char = toupper(current_char);
			if (current_char == LGDEP_KEYWORD_RED[0]) {
				RED();
			} else if (current_char == LGDEP_KEYWORD_GREEN[0]) {
				GREEN();
			} else if (current_char == LGDEP_KEYWORD_YELLOW[0]) {
				YELLOW();
			} else if (current_char == LGDEP_KEYWORD_MAGENTA[0]) {
				MAGENTA();
			} else if (current_char == LGDEP_KEYWORD_CYAN[0]) {
				CYAN();
			} else if (current_char == LGDEP_KEYWORD_WHITE[0]) {
				WHITE();
			} else if (current_char == LGDEP_KEYWORD_BLUE[0]) {	// or LGDEP_KEYWORD_BLACK[0]
				fprintf(stdout, "%c", LGDEP_KEYWORD_BLUE[0]);
				int current_char;
				while (1) {
					c = emma_getchar(&current_char);
					switch (c) {
					case _del:
					case _bs:
						return FALSE;
					case _other:
						current_char = toupper(current_char);
						if (current_char == LGDEP_KEYWORD_BLUE[1]) {	// or LGDEP_KEYWORD_BLACK[1]
							fprintf(stdout, "%c", LGDEP_KEYWORD_BLUE[1]);
							while (1) {
								c = emma_getchar(&current_char);
								switch (c) {
								case _del:
								case _bs:
									return FALSE;
								case _other:
									current_char = toupper(current_char);
									if (current_char == LGDEP_KEYWORD_BLUE[2]) {
										fprintf(stdout, "%s", LGDEP_KEYWORD_BLUE + 2);
										strcpy(color, LGDEP_KEYWORD_BLUE);
									} else if (current_char == LGDEP_KEYWORD_BLACK[2]) {
										fprintf(stdout, "%s", LGDEP_KEYWORD_BLACK + 2);
										strcpy(color, LGDEP_KEYWORD_BLACK);
									} else
										DISPLAY_BELL();
									break;
								default:
									DISPLAY_BELL();
									break;
								}
								if (*color != '\0') break;
							}
						} else
							DISPLAY_BELL();
						break;
					default:
						DISPLAY_BELL();
						break;
					}
					if (*color != '\0') break;
				}
			} else
				DISPLAY_BELL();
			break;
#endif
		default:
			DISPLAY_BELL();
			break;
		}
		if (*color != '\0') break;
	}
	return TRUE;
}
//===============================================================================
static char *getWord(char **s) {
	while (**s == ' ' || **s == '\t') ++*s;
	char *w = *s;
	while (**s != ' ' && **s != '\t' && **s != '\n' && **s != '\0') ++*s;
	**s = '\0';
	++*s;
	return w;
}
//------------------------------------------------------------------------------
static boolean is_a_direction( char *s) {
	if (strcmp(s, LGDEP_KEYWORD_LEFT) == 0 || strcmp(s, LGDEP_KEYWORD_RIGHT) == 0)
		return TRUE;
	return FALSE;
}
//------------------------------------------------------------------------------
static boolean is_a_color( char *s) {
	int i = 0;
	while (colorLabel[i][0] != '\0') {
		if (strcmp(colorLabel[i++], s) == 0) return TRUE;
	}
	return FALSE;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
#define GOFOWARD() 		emma_clear_instruction(prgline); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_GOFORWARD); \
						if (emma_read_expression(expression)) { \
							fprintf(stdout, " %s", LGDEP_KEYWORD_STEPS); \
							RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_FORWARD, LGDEP_INSTRUCTION_GOFORWARD, expression, ""); \
							emma_display_program(); \
						} else { \
							emma_clear_instruction(prgline); \
							return FALSE; \
						}
#define GOBACKWARD() 	emma_clear_instruction(prgline); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_GOBACKWARD); \
						if (emma_read_expression(expression)) { \
							fprintf(stdout, " %s", LGDEP_KEYWORD_STEPS); \
							RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_BACKWARD, LGDEP_INSTRUCTION_GOBACKWARD, expression, ""); \
							emma_display_program(); \
						} else { \
							emma_clear_instruction(prgline); \
							return FALSE; \
						}
#define SKIP()			emma_clear_instruction(prgline); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_SKIP); \
						if (emma_read_expression(expression)) { \
							fprintf(stdout, " %s", LGDEP_KEYWORD_STEPS); \
							RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_SKIP, LGDEP_INSTRUCTION_SKIP, expression, ""); \
							emma_display_program(); \
						} else { \
							emma_clear_instruction(prgline); \
							return FALSE; \
						}
#define TURN()			emma_clear_instruction(prgline); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_TURN); \
						if (get_direction(direction)) { \
							if (strcmp(direction, LGDEP_KEYWORD_LEFT) == 0) { \
								RECORD_CORRECT_INSTRUCTION2(BOARD_MSG_TURNLEFT, LGDEP_INSTRUCTION_TURNLEFT); \
							} else { \
								RECORD_CORRECT_INSTRUCTION2(BOARD_MSG_TURNRIGHT, LGDEP_INSTRUCTION_TURNRIGHT); \
							} \
							emma_display_program(); \
						} else { \
							emma_clear_instruction(prgline); \
							return FALSE; \
						}
#define ASSIGN()		emma_clear_instruction(prgline); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_ASSIGN); \
						if (emma_read_expression(expression)) { \
							fprintf(stdout, " %s ", LGDEP_KEYWORD_TO); \
							if (emma_read_variable_name(variable)) { \
								emma_create_variable_if_needed(variable); \
								RECORD_CORRECT_INSTRUCTION3(BOARD_MSG_ASSIGN, LGDEP_INSTRUCTION_ASSIGN, expression, variable); \
								emma_display_program(); \
							} else { \
								emma_clear_instruction(prgline); \
								return FALSE; \
							} \
						} else { \
							emma_clear_instruction(prgline); \
							return FALSE; \
						}
#define COLOR() 		emma_clear_instruction(prgline); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_COLOR); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_IN); \
						if (get_color(color)) { \
							RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_COLOR, LGDEP_INSTRUCTION_COLOR, color, ""); \
							emma_display_program(); \
						} else { \
							emma_clear_instruction(prgline); \
							return FALSE; \
						}
#define DO()			emma_clear_instruction(prgline); \
						fprintf(stdout, "%s ", LGDEP_KEYWORD_DO); \
						if (emma_read_expression(expression)) { \
							fprintf(stdout, " %s", LGDEP_KEYWORD_TIMES); \
							RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_DO, LGDEP_INSTRUCTION_DO, expression, ""); \
							emma_display_program(); \
						} else { \
							emma_clear_instruction(prgline); \
							return FALSE; \
						}
#define ENDDO()			emma_clear_instruction(prgline); \
						fprintf(stdout, "%s", LGDEP_KEYWORD_ENDDO); \
						RECORD_CORRECT_INSTRUCTION2(BOARD_MSG_ENDDO, LGDEP_INSTRUCTION_ENDDO); \
						emma_display_program();
//------------------------------------------------------------------------------
boolean emma_get_instruction( int c, int prgline ) {
	char direction[DIRECTION_MAX_SIZE + 1];
	char expression[EXPRESSION_MAX_SIZE + 1];
	char variable[VARIABLE_MAX_NUMBER + 1];
	char color[COLOR_MAX_SIZE + 1];

	c = toupper(c);
#ifdef FRENCH
	if (c == LGDEP_KEYWORD_GOFORWARD[0]) {
		GOFOWARD();
	} else if (c == LGDEP_KEYWORD_GOBACKWARD[0]) {
		GOBACKWARD();
	} else if (c == LGDEP_KEYWORD_GOBACKWARD[0]) {
		GOBACKWARD();
	} else if (c == LGDEP_KEYWORD_SKIP[0]) {
		SKIP();
	} else if (c == LGDEP_KEYWORD_TURN[0]) {
		TURN();
	} else if (c == LGDEP_KEYWORD_ASSIGN[0]) {
		ASSIGN();
	} else if (c == LGDEP_KEYWORD_COLOR[0]) {
		COLOR();
	} else if (c == LGDEP_KEYWORD_DO[0]) {	// or LGDEP_KEYWORD_ENDD0[0]
		emma_clear_instruction(prgline);
		fprintf(stdout, "%c", LGDEP_KEYWORD_DO[0]);
		int current_char;
		c = emma_getchar(&current_char);
		switch (c) {
		case _other:
			current_char = toupper(current_char);
			if (current_char == LGDEP_KEYWORD_DO[1]) {
				fprintf(stdout, "%s ", LGDEP_KEYWORD_DO + 1);
				if (emma_read_expression(expression)) {
					fprintf(stdout, " %s", LGDEP_KEYWORD_TIMES);
					RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_DO, LGDEP_INSTRUCTION_DO, expression, "");
					emma_display_program();
				} else {
					emma_clear_instruction(prgline);
					return FALSE;
				}
			} else if (current_char == LGDEP_KEYWORD_ENDDO[1]) {
				fprintf(stdout, "%s", LGDEP_KEYWORD_ENDDO + 1);
				RECORD_CORRECT_INSTRUCTION2(BOARD_MSG_ENDDO, LGDEP_INSTRUCTION_ENDDO);
				emma_display_program();
			} else
				DISPLAY_BELL();
			break;
		default:
			DISPLAY_BELL();
			return FALSE;
		}
	} else {
		DISPLAY_BELL();
		return FALSE;
	}
#endif

#ifdef ENGLISH
	if (c == LGDEP_KEYWORD_GOFORWARD[0]) {
		GOFOWARD();
	} else if (c == LGDEP_KEYWORD_GOBACKWARD[0]) {
		GOBACKWARD();
	} else if (c == LGDEP_KEYWORD_GOBACKWARD[0]) {
		GOBACKWARD();
	} else if (c == LGDEP_KEYWORD_SKIP[0]) {
		SKIP();
	} else if (c == LGDEP_KEYWORD_TURN[0]) {
		TURN();
	} else if (c == LGDEP_KEYWORD_ASSIGN[0]) {
		ASSIGN();
	} else if (c == LGDEP_KEYWORD_COLOR[0]) {
		COLOR();
	} else if (c == LGDEP_KEYWORD_DO[0]) {
		DO();
	} else if (c == LGDEP_KEYWORD_ENDDO[0]) {
		ENDDO();
	} else {
		DISPLAY_BELL();
		return FALSE;
	}
#endif
	return TRUE;
}
//------------------------------------------------------------------------------
void emma_read_instructions( char *instruction, int prgline ) {
	char save_instruction[INST_MAX_SIZE + 1];
	char direction[DIRECTION_MAX_SIZE + 1];
	char color[COLOR_MAX_SIZE + 1];
	boolean accuracy = TRUE;

	strcpy(save_instruction, instruction);
	char *ptw = getWord(&instruction);
	char *ptexpr;
	if (*ptw == '\0') return;
	if (strcmp(ptw, LGDEP_KEYWORD_GOFORWARD) == 0) {
		ptexpr = emma_get_expression_from_string(&instruction);
		if ( ! emma_check_expression(ptexpr)) accuracy = FALSE;
		ptw = getWord(&instruction);
		if (strcmp(ptw, LGDEP_KEYWORD_STEPS) != 0) accuracy = FALSE;
		ptw = getWord(&instruction);
		if (*ptw != '\0') accuracy = FALSE;
		if (accuracy) {
			RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_FORWARD, LGDEP_INSTRUCTION_GOFORWARD, ptexpr, "");
		} else {
			RECORD_WRONG_INSTRUCTION(BOARD_MSG_FORWARD, save_instruction);
		}
	} else {
		if (strcmp(ptw, LGDEP_KEYWORD_GOBACKWARD) == 0) {
			ptexpr = emma_get_expression_from_string(&instruction);
			if ( ! emma_check_expression(ptexpr)) accuracy = FALSE;
			ptw = getWord(&instruction);
			if (strcmp(ptw, LGDEP_KEYWORD_STEPS) != 0) accuracy = FALSE;
			ptw = getWord(&instruction);
			if (*ptw != '\0') accuracy = FALSE;
			if (accuracy) {
				RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_BACKWARD, LGDEP_INSTRUCTION_GOBACKWARD, ptexpr, "");
			} else {
				RECORD_WRONG_INSTRUCTION(BOARD_MSG_BACKWARD, save_instruction);
			}
		} else {
			if (strcmp(ptw, LGDEP_KEYWORD_SKIP) == 0) {
				ptexpr = emma_get_expression_from_string(&instruction);
				if ( ! emma_check_expression(ptexpr)) accuracy = FALSE;
				ptw = getWord(&instruction);
				if (strcmp(ptw, LGDEP_KEYWORD_STEPS) != 0) accuracy = FALSE;
				ptw = getWord(&instruction);
				if (*ptw != '\0') accuracy = FALSE;
				if (accuracy) {
					RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_SKIP, LGDEP_INSTRUCTION_SKIP, ptexpr, "");
				} else {
					RECORD_WRONG_INSTRUCTION(BOARD_MSG_SKIP, save_instruction);
				}
			} else {
				if (strcmp(ptw, LGDEP_KEYWORD_TURN) == 0) {
					ptw = getWord(&instruction);
					if ( ! is_a_direction(ptw)) accuracy = FALSE;
					strcpy(direction, ptw);
					ptw = getWord(&instruction);
					if (*ptw != '\0') accuracy = FALSE;
					if (accuracy) {
						if (strcmp(direction, LGDEP_KEYWORD_LEFT) == 0) {
							RECORD_CORRECT_INSTRUCTION2(BOARD_MSG_TURNLEFT, LGDEP_INSTRUCTION_TURNLEFT);
						} else {
							RECORD_CORRECT_INSTRUCTION2(BOARD_MSG_TURNRIGHT, LGDEP_INSTRUCTION_TURNRIGHT);
						}
					} else {
						RECORD_WRONG_INSTRUCTION(BOARD_MSG_TURNLEFT, save_instruction);
					}
				} else {
					if (strcmp(ptw, LGDEP_KEYWORD_DO) == 0) {
						ptexpr = emma_get_expression_from_string(&instruction);
						if ( ! emma_check_expression(ptexpr)) accuracy = FALSE;
						ptw = getWord(&instruction);
						if (strcmp(ptw, LGDEP_KEYWORD_TIMES) != 0) accuracy = FALSE;
						ptw = getWord(&instruction);
						if (*ptw != '\0') accuracy = FALSE;
						if (accuracy) {
							RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_DO, LGDEP_INSTRUCTION_DO, ptexpr, "");
						} else {
							RECORD_WRONG_INSTRUCTION(BOARD_MSG_DO, save_instruction);
						}
					} else {
						if (strcmp(ptw, LGDEP_KEYWORD_ENDDO) == 0) {
							ptw = getWord(&instruction);
							if (*ptw != '\0') accuracy = FALSE;
							if (accuracy) {
								RECORD_CORRECT_INSTRUCTION2(BOARD_MSG_ENDDO, LGDEP_INSTRUCTION_ENDDO);
							} else {
								RECORD_WRONG_INSTRUCTION(BOARD_MSG_ENDDO, instruction);
							}
						} else {
							if (strcmp(ptw, LGDEP_KEYWORD_ASSIGN) == 0) {
								ptexpr = emma_get_expression_from_string(&instruction);
								if ( ! emma_check_expression(ptexpr)) accuracy = FALSE;
								ptw = getWord(&instruction);
								if (strcmp(ptw, LGDEP_KEYWORD_TO) != 0) accuracy = FALSE;
								char *ptvar = emma_get_variable_name_from_string(&instruction);
								if (*ptvar == '\0')
									accuracy = FALSE;
								else {
									emma_create_variable_if_needed(ptvar);
									ptw = getWord(&instruction);
									if (*ptw != '\0') accuracy = FALSE;
								}
								if (accuracy) {
									RECORD_CORRECT_INSTRUCTION3(BOARD_MSG_ASSIGN, LGDEP_INSTRUCTION_ASSIGN, ptexpr, ptvar);
								} else {
									RECORD_WRONG_INSTRUCTION(BOARD_MSG_ASSIGN, save_instruction);
								}
							} else {
								if (strcmp(ptw, LGDEP_KEYWORD_COLOR) == 0) {
									ptw = getWord(&instruction);
									if (strcmp(ptw, LGDEP_KEYWORD_IN) != 0) accuracy = FALSE;
									ptw = getWord(&instruction);
									if ( ! is_a_color(ptw)) accuracy = FALSE;
									strcpy(color, ptw);
									ptw = getWord(&instruction);
									if (*ptw != '\0') accuracy = FALSE;
									if (accuracy) {
										RECORD_CORRECT_INSTRUCTION1(BOARD_MSG_COLOR, LGDEP_INSTRUCTION_COLOR, color, "");
									} else {
										RECORD_WRONG_INSTRUCTION(LGDEP_KEYWORD_COLOR, save_instruction);
									}
								} else {
									RECORD_WRONG_INSTRUCTION(BOARD_UNKNOWN, save_instruction);
								}
							}
						}
					}
				}
			}

		}
	}
}
//------------------------------------------------------------------------------
