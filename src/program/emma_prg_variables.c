//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
#include "emma_prg_getchar.h"
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
s_variable			variables_table[VARIABLE_MAX_NUMBER];
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void emma_clean_symbols_table( void ) {
	for (int i = 0; i < VARIABLE_MAX_NUMBER; i++) {
		variables_table[i].name[0] = '\0';
		variables_table[i].value = 0;
	}
}
//------------------------------------------------------------------------------
void emma_create_variable_if_needed( char *name ) {
	boolean found = FALSE;
	int i;
	for (i = 0; i < VARIABLE_MAX_NUMBER; i++) {
		if (variables_table[i].name[0] == '\0')
			break;
		if (strcmp(variables_table[i].name, name) == 0) {
			found = TRUE;
			break;
		}
	}
	if (i == VARIABLE_MAX_NUMBER)
		emma_fatal("%s", "Too many variables.");
	if (! found)
		strncpy(variables_table[i].name, name, VARIABLE_MAX_SIZE);
}
//------------------------------------------------------------------------------
boolean emma_read_variable_name( char *name ) {
	char *pt = name;
	int c, current_char;
	boolean end = FALSE;
	*pt = '\0';
	while (! end) {
		switch ((c = emma_getchar(&current_char))) {
		case _cr:
		case _tab:
		case _space:
			end = TRUE;
			break;
		case _bs:
		case _del:
			if (strlen(name) == 0)
				return FALSE;
			else {
				fprintf(stdout, "\b \b");
				*(--pt) = '\0';
			}
			break;
		case _other:
			if (isalpha(current_char)) {
				current_char = toupper(current_char);
				*pt = (char) current_char;
				*(++pt) = '\0';
				fprintf(stdout, "%c", current_char);
			} else
				DISPLAY_BELL();
			break;
		default:
			DISPLAY_BELL();
			break;
		}
	}
	return TRUE;
}
//------------------------------------------------------------------------------
char *emma_get_variable_name_from_string( char **s ) {
	while (**s == SPACE || **s == TAB) ++*s;
	char *w = *s;
	boolean end = FALSE;
	while (! end) {
		switch (**s) {
		case TAB:
		case SPACE:
		case CR:
			end = TRUE;
			break;
		default:
			if (isalpha(**s)) {
				**s = (char) toupper(**s);
				++*s;
			} else
				end = TRUE;
			break;
		}
	}
	**s = '\0';
	++*s;
	return w;
}
//------------------------------------------------------------------------------
boolean emma_get_variable_value( char *name, int *value ) {
	boolean found = FALSE;
	*value = 0;
	for (int i = 0; i < VARIABLE_MAX_NUMBER; i++) {
		if (strcmp(variables_table[i].name, name) == 0) {
			found = TRUE;
			*value = variables_table[i].value;
			break;
		}
	}
	return found;
}
//------------------------------------------------------------------------------
void emma_put_variable_value( char *name, int value ) {
	// The instruction has been analyzed so it must exist in the symbol table
	for (int i = 0; i < VARIABLE_MAX_NUMBER; i++) {
		if (strcmp(variables_table[i].name, name) == 0) {
			variables_table[i].value = value;
			break;
		}
	}
}
//------------------------------------------------------------------------------
