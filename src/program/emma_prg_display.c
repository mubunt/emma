//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern int 				nbLINES;
extern int 				nbCOLS;
extern char 			*screeninit;		// Startup terminal initialization
extern char 			*screendeinit;		// Exit terminal de-initialization
extern char 			*clear;				// Clear screen
extern char 			*move;				// Cursor positioning
extern int 				start_program_line;
extern int 				number_of_program_lines;
extern int 				current_program_line;
extern s_instruction	*program;
extern boolean			program_to_be_saved;
extern int 				column_status;
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void emma_display_message( const char *buff ) {
	HIDE_CURSOR();
	MOVE_CURSOR(nbLINES, 1);
	CLEAR_ENTIRE_LINE();
	MOVE_CURSOR(nbLINES, 1);
	fprintf(stdout, ESC_STRONG_ON ESC_COLOR "%s" ESC_ATTRIBUTSOFF, FOREGROUND(white), buff);
	MOVE_CURSOR(PROG_ROW, 1);
	SHOW_CURSOR();
	fflush(stdout);
}
//------------------------------------------------------------------------------
void emma_display_error( const char *buff ) {
	HIDE_CURSOR();
	MOVE_CURSOR(nbLINES, 1);
	CLEAR_ENTIRE_LINE();
	MOVE_CURSOR(nbLINES, 1);
	fprintf(stdout, ESC_STRONG_ON ESC_COLOR  "%s" ESC_ATTRIBUTSOFF, FOREGROUND(red), buff);
	MOVE_CURSOR(PROG_ROW, 1);
	SHOW_CURSOR();
	DISPLAY_BELL();
	fflush(stdout);
}
//------------------------------------------------------------------------------
void emma_display_program_status() {
	int row = nbLINES - 3;
	MOVE_CURSOR(row, column_status);
	fprintf(stdout, LGDEP_STATUS_CLEAR);
	MOVE_CURSOR(row + 1, column_status);
	fprintf(stdout, LGDEP_STATUS_CLEAR);
	if (program_to_be_saved) {
		int c = column_status + 2;
		MOVE_CURSOR(row++, c);
		fprintf(stdout, ESC_COLOR ESC_COLOR LGDEP_STATUS_MODIFIED_PROGRAM ESC_ATTRIBUTSOFF, BACKGROUND(yellow), FOREGROUND(black));
		MOVE_CURSOR(row, c);
		fprintf(stdout, ESC_COLOR ESC_COLOR LGDEP_STATUS_TO_BE_SAVED ESC_ATTRIBUTSOFF, BACKGROUND(yellow), FOREGROUND(black));
	} else {
		MOVE_CURSOR(++row, column_status);
		fprintf(stdout,  ESC_COLOR ESC_COLOR LGDEP_STATUS_PROGRAM_SAVED ESC_ATTRIBUTSOFF, BACKGROUND(green), FOREGROUND(black));
	}
	fflush(stdout);
}
//------------------------------------------------------------------------------
void emma_display_indentation( FILE *f, int n )  {
	for (int i = 0; i < n; i++) fprintf(f, "    ");
}
//------------------------------------------------------------------------------
void emma_clear_instruction( int i )  {
	MOVE_CURSOR(start_program_line + i, START_INSTRUCTION);
	fprintf(stdout, "                                     ");
	MOVE_CURSOR(start_program_line + i, START_INSTRUCTION);
}
//------------------------------------------------------------------------------
int emma_display_program( void ) {
	int indent = 0;
	for (int i = 0; i < number_of_program_lines; i++) {
		emma_clear_instruction(i);
		if (program[i].instruction[0] != '\0') {
			if (strcmp(program[i].action, BOARD_MSG_ENDDO) == 0 && indent > 0) --indent;
			emma_display_indentation(stdout, indent);
			if (program[i].accuracy)
				fprintf(stdout, "%s", program[i].instruction);
			else
				fprintf(stdout, "%s  " ESC_STRONG_ON ESC_COLOR LGDEP_ERRONEOUS_INSTRUCTION ESC_ATTRIBUTSOFF, program[i].instruction, FOREGROUND(red));
			if (strcmp(program[i].action, BOARD_MSG_DO) == 0) ++indent;
		}
	}
	return indent;
}
//------------------------------------------------------------------------------
