//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
#include "emma_prg_getchar.h"
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	size_t 	nbFiles;
	size_t	maxLenFileNames;
	char 	**ptFiles;
} file_list;
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern int 					nbLINES;
extern int 					nbCOLS;
extern char 				*screeninit;	// Startup terminal initialization
extern char 				*screendeinit;	// Exit terminal de-initialization
extern char 				*clear;			// Clear screen
extern char 				*move;			// Cursor positioning
extern s_instruction		*program;
extern int 					number_of_program_lines;
extern char					cwd[];			// Current directory
extern boolean				program_to_be_saved;
extern int 					current_program_line;
extern const char			*messages[];
extern char 				loaded_file[];
extern char 				loaded_file_description[];
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int 	row_of_first_value;
static int 	current_first_value_displayed;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void display_save_panel(int l, int c) {
	int row = l;
	HIDE_CURSOR();
	MOVE_CURSOR(row++, c);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE1 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, c);
	if (strlen(cwd) <= 45) {
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE2 ESC_ATTRIBUTSOFF, FOREGROUND(yellow), cwd);
		MOVE_CURSOR(row++, c);
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE31 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	} else {
		char cc = cwd[45];
		cwd[45] = '\0';
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE2 ESC_ATTRIBUTSOFF, FOREGROUND(yellow), cwd);
		cwd[45] =cc;
		MOVE_CURSOR(row++, c);
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE32 ESC_ATTRIBUTSOFF, FOREGROUND(yellow), cwd + 45);
	}
	MOVE_CURSOR(row++, c);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE4 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, c);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE5 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, c);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_SAVE_LINE6 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));

	const char *ptr = strstr(LGDEP_SAVE_LINE4, "_");
	if (ptr) c += (int) (ptr - LGDEP_SAVE_LINE4 - 2);
	MOVE_CURSOR(l + 3, c);
	fprintf(stdout, "%s", loaded_file);

	MOVE_CURSOR(l + 4, c);
	fprintf(stdout, "%s", loaded_file_description);

	fflush(stdout);
}
//------------------------------------------------------------------------------
static void read_save_file(int row, int column, char *file, size_t len) {
	row = row + 3;
	const char *ptr = strstr(LGDEP_SAVE_LINE4, "_");
	if (ptr) column += (int) (ptr - LGDEP_SAVE_LINE4 - 2);

	strcpy(file, loaded_file);
	char *ptfile = file + strlen(file);

	SHOW_CURSOR();
	MOVE_CURSOR(row, column + (int) strlen(file));
	int current_char;
	boolean stop = FALSE;
	while (! stop) {
		char c = (char) emma_getchar(&current_char);
		switch (c) {
		case _cr:
			stop = TRUE;
			break;
		case _bs:
		case _del:
			if (ptfile == file)
				DISPLAY_BELL();
			else {
				*(--ptfile) = '\0';
				fprintf(stdout, "\b_\b");
			}
			break;
		case _space:
		case _other:
			c = (char)current_char;
			if (isalpha(c) || isdigit(c) || c == '-' || c == '_') {
				if (len <= (size_t) (ptfile - file))
					DISPLAY_BELL();
				else {
					*ptfile++ = c;
					*ptfile = '\0';
					fprintf(stdout, "%c", c);
				}
			} else
				DISPLAY_BELL();
			break;
		default:
			DISPLAY_BELL();
			break;
		}
	}
	strcpy(loaded_file, file);
}
//------------------------------------------------------------------------------
static void read_save_description(int row, int column, char *descr, size_t len) {
	row = row + 4;
	const char *ptr = strstr(LGDEP_SAVE_LINE5, "_");
	if (ptr) column += (int) (ptr - LGDEP_SAVE_LINE5 - 2);

	strcpy(descr, loaded_file_description);
	char *ptdescr = descr + strlen(descr);

	SHOW_CURSOR();
	MOVE_CURSOR(row, column + (int) strlen(descr));
	int current_char;
	boolean stop = FALSE;
	while (! stop) {
		char c = (char) emma_getchar(&current_char);
		switch (c) {
		case _cr:
			stop = TRUE;
			break;
		case _bs:
		case _del:
			if (ptdescr == descr)
				DISPLAY_BELL();
			else {
				*(--ptdescr) = '\0';
				fprintf(stdout, "\b_\b");
			}
			break;
		case _space:
		case _other:
			if (len <= (size_t) (ptdescr - descr))
				DISPLAY_BELL();
			else {
				*ptdescr++ = (char) current_char;
				*ptdescr = '\0';
				fprintf(stdout, "%c", current_char);
			}
			break;
		default:
			DISPLAY_BELL();
			break;
		}
	}
	strcpy(loaded_file_description, descr);
}
//------------------------------------------------------------------------------
// FILE CHOOSER
//------------------------------------------------------------------------------
static void display_a_file( int row, int column, size_t idx, file_list *files, char **descrs ) {
	MOVE_CURSOR(row, column);
	if (idx < files->nbFiles)
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE4 ESC_ATTRIBUTSOFF, FOREGROUND(yellow), files->ptFiles[idx], descrs[idx]);
	else
		fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE4 ESC_ATTRIBUTSOFF, FOREGROUND(yellow), " ", " ");
}

static void display_file_chooser( int row, int column, size_t nb_values_displayed, file_list *files, char **descrs ) {
	HIDE_CURSOR();
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE1 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE2 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE3 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	row_of_first_value = row;
	for (size_t i = 0; i < nb_values_displayed; i++ ) display_a_file(row++, column, i, files, descrs);
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE5 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, column);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE6 ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	fflush(stdout);
}

static int compare(const void  *a, const void *b) {
	return strcmp(* (char *const *) a, * (char *const *) b);
}

static const char *get_file_suffix(const char *filename) {
	const char *dot = strrchr(filename, '.');
	if(!dot) return "";
	return dot;
}

static file_list *fileSelector(char *directory) {
	DIR *pDir;
	struct dirent *pDirent;
	struct stat sb;
	char object[PATH_MAX + 1];

	if ((pDir = opendir(directory))  == NULL) emma_fatal("Cannot open directory '%s'", directory);
	// Step 1: Count the number of directories and regular files.
	size_t nbFiles = 0;
	size_t maxLenFileNames = 0;
	while ((pDirent = readdir(pDir)) != NULL) {
		if (strlen(pDirent->d_name) == 0) continue;
		if (pDirent->d_name[0] == '.') continue;
		sprintf(object, "%s/%s", directory, pDirent->d_name);
		if (lstat(object, &sb) < 0) emma_fatal("Cannot stat %s\n", object);
		mode_t mode = sb.st_mode & S_IFMT;
		if (mode == S_IFREG) {
			if (strcmp(EMMA_SUFFIX, get_file_suffix(pDirent->d_name)) == 0) {
				++nbFiles;
				maxLenFileNames = MAX(maxLenFileNames, strlen(pDirent->d_name) + 1);
			}
		}
	}
	rewinddir(pDir);
	// Step 2: Allocate structure and table for directories and regular files and fill them.
	file_list *ptfile_list  = malloc(sizeof(file_list));
	ptfile_list->maxLenFileNames = maxLenFileNames;
	ptfile_list->nbFiles = nbFiles;
	ptfile_list->ptFiles = malloc(nbFiles * sizeof(char *));
	if (nbFiles != 0) {
		size_t indexFiles = 0;
		while ((pDirent = readdir(pDir)) != NULL) {
			if (strlen(pDirent->d_name) == 0) continue;
			if (pDirent->d_name[0] == '.') continue;
			sprintf(object, "%s/%s", directory, pDirent->d_name);
			lstat(object, &sb);
			mode_t mode = sb.st_mode & S_IFMT;
			if (mode == S_IFREG) {
				if (strcmp(EMMA_SUFFIX, get_file_suffix(pDirent->d_name)) == 0) {
					ptfile_list->ptFiles[indexFiles] = malloc(maxLenFileNames * sizeof(char));
					strcpy(ptfile_list->ptFiles[indexFiles], pDirent->d_name);
					ptfile_list->ptFiles[indexFiles][strlen(ptfile_list->ptFiles[indexFiles]) - strlen(EMMA_SUFFIX)] = '\0';
					++indexFiles;
				}
			}
		}
		// Step 3: Sort these tables alphabetically
		qsort(ptfile_list->ptFiles, ptfile_list->nbFiles, sizeof(char *), compare);
	}
	closedir(pDir);
	return(ptfile_list);
}

static void free_fileSelector( file_list *pt, char **pd ) {
	if (pt != NULL) {
		size_t n = pt->nbFiles;
		for (size_t i = 0; i < n; ++i) free(pt->ptFiles[i]);
		free(pt->ptFiles);
		free(pt);

		if (pd != NULL) {
			for (size_t i = 0; i < n; ++i) free(pd[i]);
			free(pd);
		}
	}
}

static char **getDescription( file_list *files ) {
	char buffer[256];
	char fn[PATH_MAX + 1];
	char **desc = malloc(files->nbFiles * sizeof(char *));

	for (size_t i = 0; i < files->nbFiles; ++i) {
		desc[i] = malloc((DESCR_MAX_SIZE + 2) * sizeof(char));
		strcpy(desc[i], "");
		sprintf(fn, "%s%s", files->ptFiles[i], EMMA_SUFFIX);
		FILE *fp = fopen(fn, "r");
		if (fp == NULL) continue;
		char *p;
		do {
			if ((p = fgets(buffer, sizeof(buffer), fp)) != NULL)
				if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
		} while (p != NULL && buffer[0] == '\0');
		fclose(fp);
		if (p == NULL) continue;
		if (strncmp(buffer, TAGDESCR, strlen(TAGDESCR)) == 0)
			strncpy(desc[i], buffer + strlen(TAGDESCR), DESCR_MAX_SIZE);
	}
	return desc;
}

#define HIGHLIGHT(n)	MOVE_CURSOR(row_of_first_value + n, column); \
						fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│  │ " ESC_REVERSE_ON "%-20s  %-32s" ESC_ATTRIBUTSOFF ESC_COLOR ESC_STRONG_ON " ▒ │  │", \
						FOREGROUND(yellow), \
						emmafiles->ptFiles[current_first_value_displayed + n], emmadescriptions[current_first_value_displayed + n], \
						FOREGROUND(yellow)); \
						fflush(stdout);

#define UNHIGHLIGHT(n)	MOVE_CURSOR(row_of_first_value + n, column); \
						fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_LOAD_LINE4, \
						FOREGROUND(yellow), \
						emmafiles->ptFiles[current_first_value_displayed + n], emmadescriptions[current_first_value_displayed + n]); \
						fflush(stdout);

static void file_chooser( int row, int column, size_t nb_values_displayed, char *filename, char *description ) {
	char buffer[256];
	file_list *emmafiles = fileSelector(cwd);
	if (emmafiles->nbFiles == 0) {
		free_fileSelector(emmafiles, NULL);
		strcpy(filename, "");
		emma_display_error(messages[I_NO_FILE_TO_LOAD]);
		return;
	}
	char **emmadescriptions = getDescription(emmafiles);

	current_first_value_displayed = 0;
	display_file_chooser(row, column, nb_values_displayed, emmafiles, emmadescriptions);
	int selection = 0;
	int status = 0;
	HIGHLIGHT(selection);
	int current_char;
	while (1) {
		int c = emma_getchar(&current_char);
		switch (c) {
		case _cr:
		case _tab:
			status = 1;
			break;
		case _bs:
		case _del:
			status = 2;
			break;
		case _arrow_up:
			if (selection == 0) {
				if (current_first_value_displayed == 0)
					DISPLAY_BELL();
				else  {
					UNHIGHLIGHT(selection);
					--current_first_value_displayed;
					for (size_t i = 0; i < nb_values_displayed; i++)
						display_a_file(row_of_first_value + (int) i, column,
						               (size_t) current_first_value_displayed + i, emmafiles, emmadescriptions);
					HIGHLIGHT(selection);
				}
			} else {
				UNHIGHLIGHT(selection);
				--selection;
				HIGHLIGHT(selection);
			}
			break;
		case _arrow_down:
			if (selection == (int) (emmafiles->nbFiles % nb_values_displayed) - 1) {
				if ((current_first_value_displayed + selection) == (int) emmafiles->nbFiles - 1)
					DISPLAY_BELL();
				else  {
					UNHIGHLIGHT(selection);
					++current_first_value_displayed;
					size_t  i;
					for (i = 0; i < nb_values_displayed; i++)
						display_a_file(row_of_first_value + (int) i, column,
						               (size_t) current_first_value_displayed + i, emmafiles, emmadescriptions);
					selection = (int) i - 1;
					HIGHLIGHT(selection);
				}
			} else {
				UNHIGHLIGHT(selection);
				++selection;
				HIGHLIGHT(selection);
			}
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (status != 0) break;
	}
	if (status == 1) {
		strcpy(filename, emmafiles->ptFiles[current_first_value_displayed + selection]);
		strcpy(description, emmadescriptions[current_first_value_displayed + selection]);
	} else {
		strcpy(filename, "");
		strcpy(description, "");
	}
	free_fileSelector(emmafiles, emmadescriptions);
}
//==============================================================================
static boolean save_program( char *filename, char *description ) {
	char fn[PATH_MAX + 1];
	sprintf(fn, "%s%s", filename, EMMA_SUFFIX);
	FILE *fp = fopen(fn, "w");
	if (fp == NULL) return FALSE;
	fprintf(fp, "%s%s\n", TAGDESCR, description);
	int indent = 0;
	for (int i = 0; i < number_of_program_lines; i++) {
		if (program[i].instruction[0] != '\0') {
			if (strcmp(program[i].action, BOARD_MSG_ENDDO) == 0 && indent > 0) --indent;
			emma_display_indentation(fp, indent);
			fprintf(fp, "%s\n", program[i].instruction);
			if (strcmp(program[i].action, BOARD_MSG_DO) == 0) ++indent;
		}
	}
	fclose(fp);
	return TRUE;
}
//------------------------------------------------------------------------------
static int read_program( char *filename ) {
	char fn[PATH_MAX + 1];
	char buffer[1024];
	sprintf(fn, "%s%s", filename, EMMA_SUFFIX);
	FILE *fp = fopen(fn, "r");
	if (fp == NULL) return 1;
	int l = 0;
	boolean b;
	while (fgets(buffer, sizeof(buffer), fp) != NULL)  {
		if (strncmp(buffer, TAGDESCR, strlen(TAGDESCR)) == 0) continue;
		if (buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
		char *s = buffer;
		while (*s == ' ' || *s == '\t') ++s;
		if (strlen(s) == 0) continue;
		if (l == number_of_program_lines) {
			fclose(fp);
			return 2;
		}
		emma_read_instructions(s, l++);
	}
	fclose(fp);
	return 0;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void emma_save( void ) {
	char filename[FILENAME_MAX_SIZE + 2];
	char description[DESCR_MAX_SIZE + 2];
	int row = 20;
	int column = 10;
	display_save_panel(row, column);
	while (1) {
		read_save_file(row, column, filename, FILENAME_MAX_SIZE);
		if (strlen(filename) == 0) {
			emma_refresh();
			break;
		}
		read_save_description(row, column, description, DESCR_MAX_SIZE);
		if (save_program(filename, description)) {
			program_to_be_saved = FALSE;
			emma_refresh();
			break;
		}
		emma_refresh();
		char buff[PATH_MAX];
		sprintf(buff, messages[I_CANNOT_CREATE], filename);
		emma_display_error(buff);
	}
}
//------------------------------------------------------------------------------
void emma_load( void ) {
	char buff[PATH_MAX + 1];
	char filename[FILENAME_MAX_SIZE + 1];
	char description[DESCR_MAX_SIZE + 1];
	int row = 15;
	int column = 10;
	size_t number_of_values_displayed = 10;
	file_chooser(row, column, number_of_values_displayed, filename, description);
	if (strlen(filename) != 0) {
		emma_clear_program(program, number_of_program_lines);
		switch (read_program(filename)) {
		case 0:
			strcpy(loaded_file, filename);
			strcpy(loaded_file_description, description);
			program_to_be_saved = FALSE;
			current_program_line = 0;
			emma_refresh();
			break;
		case 1:
			strcpy(loaded_file, "");
			strcpy(loaded_file_description, "");
			emma_refresh();
			sprintf(buff, messages[I_CANNOT_READ], filename);
			emma_display_error(buff);
			break;
		case 2:
			strcpy(loaded_file, "");
			strcpy(loaded_file_description, "");
			emma_refresh();
			sprintf(buff, messages[I_TOO_MANY_INSTRUCTIONS], number_of_program_lines);
			emma_display_error(buff);
			break;
		}
	} else
		emma_refresh();
}
//------------------------------------------------------------------------------
