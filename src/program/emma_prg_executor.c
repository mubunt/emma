//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define IPCKEY					getpid()	// IPC key for this client

#define DISPLAY_EXECCURSOR(k)	MOVE_CURSOR(start_program_line + k, 1); \
								fprintf(stdout, EXECCURSOR); \
								fflush(stdout);
#define CLEAR_EXECCURSOR(k)		MOVE_CURSOR(start_program_line + k, 1); \
								fprintf(stdout, NOCURSOR); \
								fflush(stdout);
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern int 						nbCOLS;
extern char 					*move;				// Cursor positioning
extern int 						start_program_line;
extern int 						number_of_program_lines;
extern s_instruction			*program;
extern int						board_request;	// request identificator for board
extern int						board_response;	// response identificator for board
extern s_msg					msg;			// IPC message
extern const char				*messages[];
extern const char				*colorLabel[];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static int execute_assign( int inst ) {
	int result;
	if (! emma_evaluate_expression(program[inst].parameter, &result)) {
		emma_display_error(messages[I_CANNOT_EVALUATE_EXPRESSION]);
		return -1;
	}
	emma_put_variable_value(program[inst].variable, result);
	return inst;
}

static int execute_instruction( int inst ) {
	if (strcmp(program[inst].action, BOARD_MSG_FORWARD) == 0
	        || strcmp(program[inst].action, BOARD_MSG_BACKWARD) == 0
	        || strcmp(program[inst].action, BOARD_MSG_SKIP) == 0) {
		int result;
		if (! emma_evaluate_expression(program[inst].parameter, &result)) {
			emma_display_error(messages[I_CANNOT_EVALUATE_EXPRESSION]);
			return -1;
		}
		SEND_COMMAND2(program[inst].action, result);
	} else {
		if (strcmp(program[inst].action, BOARD_MSG_TURNRIGHT) == 0
		        || strcmp(program[inst].action, BOARD_MSG_TURNLEFT) == 0) {
			SEND_COMMAND1(program[inst].action);
		} else {
			int i = 0;
			while (strcmp(colorLabel[i], program[inst].parameter) != 0) ++i;
			SEND_COMMAND2(program[inst].action, i);
		}
	}
	if (strncmp(msg.mtext, ACKNOWLEDGMENT, MSG_MAX_SIZE) != 0) {
		if (strncmp(msg.mtext, WARNING_CRASH, MSG_MAX_SIZE) == 0)
			emma_display_error(messages[I_EXEC_CRASH]);
		else
			emma_display_error(messages[I_EXEC_UNKNOWN]);
		return -1;
	}
	return inst;
}

static int execute_do( int inst ) {
	int idx_enddo = 0;
	CLEAR_EXECCURSOR(inst);
	int i = inst + 1;
	int j, result;
	if (! emma_evaluate_expression(program[inst].parameter, &result)) {
		emma_display_error(messages[I_CANNOT_EVALUATE_EXPRESSION]);
		i = -1;
		return i;
	}
	program[inst].counter = program[inst].counter_initial = result;
	while (program[inst].counter) {
		j = i;
		DISPLAY_EXECCURSOR(i);
		if (! program[i].accuracy) {
			emma_display_error(messages[I_PROGRAM_WITH_ERROR]);
			i = -1;
			break;
		}
		if (program[i].action[0] != '\0') {
			if (strcmp(program[i].action, BOARD_MSG_ASSIGN) == 0)
				i = execute_assign(i);
			else {
				if (strcmp(program[i].action, BOARD_MSG_ENDDO) == 0) {
					--program[inst].counter;
					idx_enddo = i;
					i = inst;
				} else {
					if (strcmp(program[i].action, BOARD_MSG_DO) == 0)
						i = execute_do(i);
					else
						i = execute_instruction(i);
				}
			}
		}
		if (i == -1) break;
		CLEAR_EXECCURSOR(j);
		++i;
	}
	if (i != -1) i = idx_enddo;
	return i;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void emma_executor( void ) {
	boolean exec = TRUE;
	emma_display_error(messages[I_EXECUTION_ONGOING]);
	HIDE_CURSOR();
	SEND_COMMAND1(BOARD_MSG_HOME);
	int i = 0;
	while (i < number_of_program_lines) {
		DISPLAY_EXECCURSOR(i);
		if (! program[i].accuracy) {
			emma_display_error(messages[I_PROGRAM_WITH_ERROR]);
			i = -1;
			break;
		}
		if (program[i].action[0] != '\0') {
			if (strcmp(program[i].action, BOARD_MSG_ASSIGN) == 0)
				i = execute_assign(i);
			else {
				if (strcmp(program[i].action, BOARD_MSG_DO) == 0)
					i = execute_do(i);
				else
					i = execute_instruction(i);
			}
		}
		if (i == -1) break;
		CLEAR_EXECCURSOR(i);
		++i;
	}
	if (i != -1) {
		SEND_COMMAND1(BOARD_MSG_END);
		emma_display_message(messages[I_EXECUTION_ENDED]);
		HIDE_CURSOR();
	}
}
//------------------------------------------------------------------------------
