//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define IPCKEY						getpid()	// IPC key for this client

#define DISPLAY_BOTTOM_LINE()		MOVE_CURSOR(nbLINES - 1, 1); \
									fprintf(stdout, ESC_COLOR ESC_STRONG_ON, FOREGROUND(yellow)); \
									for (int i = 0; i < nbCOLS; i++) fprintf(stdout, "─"); \
									fprintf(stdout, ESC_ATTRIBUTSOFF);

TRACYFD_DEFINITION
//------------------------------------------------------------------------------
// EXPORTABLE VARIABLES
//------------------------------------------------------------------------------
int 				nbLINES;
int 				nbCOLS;
char				cwd[PATH_MAX];								// Current directory
char 				*screeninit;								// Startup terminal initialization
char 				*screendeinit;								// Exit terminal de-initialization
char 				*clear;										// Clear screen
char 				*move;										// Cursor positioning
int 				start_program_line;
int 				number_of_program_lines;
int 				current_program_line;
s_instruction		*program = NULL;
boolean				program_to_be_saved;
int 				column_status;
char 				loaded_file[PATH_MAX];							// current file in memory
char 				loaded_file_description[DESCR_MAX_SIZE + 1];	// description of current file in memory
int					board_request;									// request identificator for board
int					board_response;									// response identificator for board
s_msg				msg;											// IPC message
const char			*messages[]	= {
	LGDEP_WAITING_PROGRAM,
	LGDEP_EXECUTION_ONGOING,
	LGDEP_EXECUTION_ENDED,
	LGDEP_EXEC_CRASH,
	LGDEP_EXEC_FATAL_1,
	LGDEP_EXEC_FATAL_2,
	LGDEP_EXEC_UNKNOWN,
	LGDEP_SYNTAX_ERROR,
	LGDEP_CANNOT_CREATE,
	LGDEP_CANNOT_READ,
	LGDEP_PROGRAM_WITH_ERROR,
	LGDEP_TOO_MANY_INSTRUCTIONS,
	LGDEP_NO_FILE_TO_LOAD,
	LGDEP_CANNOT_EVALUATE_EXPRESSION,
	""
};
const char 			*colorLabel[] = {
	LGDEP_KEYWORD_BLACK,
	LGDEP_KEYWORD_RED,
	LGDEP_KEYWORD_GREEN,
	LGDEP_KEYWORD_YELLOW,
	LGDEP_KEYWORD_BLUE,
	LGDEP_KEYWORD_MAGENTA,
	LGDEP_KEYWORD_CYAN,
	LGDEP_KEYWORD_WHITE,
	""
};
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int 			column_explanation;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void desktop_init( void ) {
	CLEAR_ENTIRE_SCREEN();

	int row = 1;
	MOVE_CURSOR(row++, 1);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON LGDEP_WELCOME ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, 1);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╰" ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	for (int i = 2; i < nbCOLS - 18; i++) fprintf(stdout, ESC_COLOR ESC_STRONG_ON "─" ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON "╮" ESC_ATTRIBUTSOFF, FOREGROUND(yellow));
	MOVE_CURSOR(row++, nbCOLS - 18);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON "│" ESC_ATTRIBUTSOFF, FOREGROUND(yellow));

	LGDEP_PROGRAM_ELEMENTS();
	DISPLAY_BOTTOM_LINE();

	MOVE_CURSOR(PROG_ROW - 1, 1);
	fprintf(stdout, ESC_STRONG_ON LGDEP_YOURPROGRAM ESC_ATTRIBUTSOFF);

	start_program_line = PROG_ROW;
	int end_program_line = nbLINES - 3;
	number_of_program_lines = end_program_line - start_program_line + 1;
	for (int i = 0; i < number_of_program_lines; i++) {
		MOVE_CURSOR(start_program_line + i, 1);
		fprintf(stdout, "  %2d  ", i + 1);
	}

	fflush(stdout);
}
//------------------------------------------------------------------------------
static int start_board( void ) {
	int ierror = 0;
	SEND_COMMAND1(BOARD_MSG_START);
	if (strcmp(msg.mtext, ACKNOWLEDGMENT) != 0) {
		if (strncmp(msg.mtext, ERROR_CPPOTCE, MSG_MAX_SIZE) == 0)
			ierror = I_EXEC_FATAL_1;
		else if (strncmp(msg.mtext, ERROR_SDDNE, MSG_MAX_SIZE) == 0)
			ierror = I_EXEC_FATAL_2;
		else
			ierror = I_EXEC_UNKNOWN;
	}
	return ierror;
}
//------------------------------------------------------------------------------
static s_instruction *alloc_program( int nbinst ) {
	s_instruction *p ;
	if (NULL == (p = malloc( sizeof(s_instruction) * (size_t) nbinst)))
		emma_fatal("%s", "No more memory space avalaible for allocation. Abort.");
	emma_clear_program(p, nbinst);
	return p;
}
//------------------------------------------------------------------------------
static void free_program( void ) {
	if (program != NULL) {
		free(program);
		program = NULL;
	}
}
//------------------------------------------------------------------------------
static void windowsizemanager( int sig ) {
	signal(SIGWINCH, SIG_IGN);
	emma_getTerminalSize(&nbLINES, &nbCOLS);
	column_explanation = nbCOLS - DIFFERENTIAL_FOR_THE_INSTRUCTIONS_COLUMN;
	column_status = nbCOLS - DIFFERENTIAL_FOR_THE_STATUS_COLUMN;
	emma_refresh();
	MOVE_CURSOR(start_program_line, START_INSTRUCTION);
	fflush(stdout);
	signal(SIGWINCH, windowsizemanager);
}
//------------------------------------------------------------------------------
static void interruptmanager( int sig ) {
	signal(SIGHUP, SIG_IGN);
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
//	signal(SIGKILL, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	SEND_COMMAND1(BOARD_MSG_STOP);
	emma_cleanContext();
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void emma_cleanContext( void ) {
	// IPC deletion
	DELETEIPC(board_request);
	DELETEIPC(board_response);
	// Restore screen
	emma_endScreen();
	// Free memory
	free_program();
	// Restore signals
	signal(SIGHUP, SIG_DFL);
	signal(SIGABRT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGINT, SIG_DFL);
//	signal(SIGKILL, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGWINCH, SIG_DFL);
}
//------------------------------------------------------------------------------
void emma_clear_program( s_instruction *p, int nbinst ) {
	for (int i = 0; i < nbinst; i++) {
		p[i].counter = 0;
		p[i].instruction[0] = p[i].action[0] = p[i].parameter[0] = '\0';
		p[i].accuracy = TRUE;
	}
}
//------------------------------------------------------------------------------
void emma_refresh( void ) {
	desktop_init();
	emma_display_program();
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main( int argc, char **argv ) {
	_TRACY_START(_TRACYPROGRAM);
	// Signals
	signal(SIGHUP, SIG_IGN);
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
//	signal(SIGKILL, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGWINCH, SIG_IGN);
	// Get rows and cols number of the terminal
	int opt;
	while ((opt = getopt(argc, argv, "r:c:")) != -1) {
		switch (opt) {
		case 'r':
			nbLINES = atoi(optarg);
			break;
		case 'c':
			nbCOLS = atoi(optarg);
			break;
		default:
			emma_fatal("Usage: %s -r rows -c columns\n", argv[0]);
		}
	}
	if (nbLINES == 0 || nbCOLS == 0)
		emma_fatal("Usage: %s -r rows -c columns\n", argv[0]);
	// Initialize screen
	emma_initScreen();
	column_explanation = nbCOLS - DIFFERENTIAL_FOR_THE_INSTRUCTIONS_COLUMN;
	column_status = nbCOLS - DIFFERENTIAL_FOR_THE_STATUS_COLUMN;
	// Get current directory
	if (getcwd(cwd, sizeof(cwd)) == NULL)
		emma_fatal("Cannot get current directoty (errno='%s')", strerror(errno));
	// IPC Creation
	CREATEIPC(PROGRAM, BOARD, board_request, BOARD_REQUEST);
	CREATEIPC(PROGRAM, BOARD, board_response, BOARD_RESPONSE);
	msg.mtype = IPCKEY;
	// Interrupts signals
	signal(SIGHUP, interruptmanager);
	signal(SIGABRT, interruptmanager);
	signal(SIGTERM, interruptmanager);
	signal(SIGINT, interruptmanager);
//	signal(SIGKILL, interruptmanager);
	signal(SIGQUIT, interruptmanager);
	// Window change size signal management
	signal(SIGWINCH, windowsizemanager);
	// Starting board...
	int ierror;
	if ((ierror = start_board()) != 0) {
		emma_display_error(messages[ierror]);
		SEND_COMMAND1(BOARD_MSG_STOP);
		emma_cleanContext();
		emma_fatal("%s", messages[ierror]);
		return(EXIT_FAILURE);
	}
	while (1) {
		// Display general frame
		desktop_init();
		// Processing loop
		strcpy(loaded_file, "");
		strcpy(loaded_file_description, "");
		program = alloc_program(number_of_program_lines);
		emma_clean_symbols_table();
		current_program_line = 0;
		int state;
		emma_display_message(messages[I_WAITING_PROGRAM]);
		program_to_be_saved = FALSE;
		while (1) {
			state = emma_editor();
			if (state == 0) {	// 0	Stop and exit
				if (program_to_be_saved) {
					if (emma_getConfirmationTheProgramNeedsToBeSaved())
						emma_save();
				}
				SEND_COMMAND1(BOARD_MSG_STOP);
				if (strncmp(msg.mtext, ACKNOWLEDGMENT, MSG_MAX_SIZE) != 0) {
					emma_display_error(messages[I_EXEC_UNKNOWN]);
				}
				break;
			}
			if (state == 1) {	// 1	Clean
				SEND_COMMAND1(BOARD_MSG_RESTART);
				if (strncmp(msg.mtext, ACKNOWLEDGMENT, MSG_MAX_SIZE) != 0) {
					emma_display_error(messages[I_EXEC_UNKNOWN]);
				}
				break;
			}
			if (state == 2) {	// 2	Run
				emma_executor();
				continue;
			}
			if (state == 3) {	// 3	Save
				emma_save();
				continue;
			}
			if (state == 4) {	// 4	Load
				emma_load();
				continue;
			}
		}
		free_program();
		if (state == 0) break;
		emma_clean_symbols_table();
	}
	// Exit
	emma_cleanContext();
	_TRACY_STOP();
	return(EXIT_SUCCESS);
}
//------------------------------------------------------------------------------
