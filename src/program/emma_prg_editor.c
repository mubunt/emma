//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
#include "emma_prg_getchar.h"
//------------------------------------------------------------------------------
// EXTERNAL VARIABLES
//------------------------------------------------------------------------------
extern int 						nbLINES;
extern int 						nbCOLS;
extern char 					*screeninit;		// Startup terminal initialization
extern char 					*screendeinit;		// Exit terminal de-initialization
extern char 					*clear;				// Clear screen
extern char 					*move;				// Cursor positioning
extern int 						start_program_line;
extern int 						number_of_program_lines;
extern int 						current_program_line;
extern s_instruction			*program;
extern boolean					program_to_be_saved;
extern const char				*messages[];
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void shiftdown_instruction( int prgline ) {
	for (int i = number_of_program_lines - 1; i > prgline; i--) {
		program[i].counter_initial = program[i - 1].counter_initial;
		program[i].counter = program[i - 1].counter;
		program[i].accuracy = program[i - 1].accuracy;
		strcpy(program[i].instruction, program[i - 1].instruction);
		strcpy(program[i].action, program[i - 1].action);
		strcpy(program[i].parameter, program[i - 1].parameter);
		strcpy(program[i].variable, program[i - 1].variable);
	}
	program[prgline].counter_initial = program[prgline].counter = 0;
	program[prgline].instruction[0] = program[prgline].action[0] = '\0';
	program[prgline].parameter[0] = program[prgline].variable[0] = '\0';
	program[prgline].accuracy = TRUE;
}
//------------------------------------------------------------------------------
static void shiftup_instruction( int prgline ) {
	for (int i = prgline; i < number_of_program_lines; i++) {
		program[i].counter_initial = program[i + 1].counter_initial;
		program[i].counter = program[i + 1].counter;
		program[i].accuracy = program[i + 1].accuracy;
		strcpy(program[i].instruction, program[i + 1].instruction);
		strcpy(program[i].action, program[i + 1].action);
		strcpy(program[i].parameter, program[i + 1].parameter);
		strcpy(program[i].variable, program[i + 1].variable);
	}
	program[number_of_program_lines - 1].counter_initial = program[number_of_program_lines].counter = 0;
	program[number_of_program_lines - 1].instruction[0] = program[number_of_program_lines - 1].action[0] = '\0';
	program[number_of_program_lines - 1].parameter[0] = program[number_of_program_lines - 1].variable[0] = '\0';
	program[number_of_program_lines - 1].accuracy = TRUE;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
int emma_editor( void ) {
	int goon = -1;
	int previous_program_line = current_program_line;
	SHOW_CURSOR();
	while (goon < 0) {
		if (previous_program_line != current_program_line) {
			MOVE_CURSOR(start_program_line + previous_program_line, 1);
			fprintf(stdout, NOCURSOR);
		}
		MOVE_CURSOR(start_program_line + current_program_line, 1);
		fprintf(stdout, CURSOR);
		MOVE_CURSOR(start_program_line + current_program_line, START_INSTRUCTION);
		fflush(stdout);

		previous_program_line = current_program_line;
		int current_char;
		int c = emma_getchar(&current_char);
		switch (c) {
		case _f2:	// Run
			if (emma_display_program() != 0) {
				emma_display_error(messages[I_SYNTAX_ERROR]);
				DISPLAY_BELL();
			} else
				goon = 2;
			break;
		case _f4:	// Clean
			goon = 1;
			break;
		case _f6:	// Save
			goon = 3;
			break;
		case _f7:	// Load
			goon = 4;
			break;
		case _f9:	// Exit
			goon = 0;
			break;
		case _suppr:	// Suppr
			shiftup_instruction(current_program_line);
			emma_display_program();
			program_to_be_saved = TRUE;
			emma_display_program_status();
			break;
		case _arrow_up:
			if (current_program_line == 0)
				DISPLAY_BELL();
			else
				current_program_line--;
			break;
		case _arrow_down:
			if (current_program_line == number_of_program_lines - 1)
				DISPLAY_BELL();
			else
				++current_program_line;
			break;
		case _cr:
			if (*(program[number_of_program_lines - 1].instruction) != '\0')
				DISPLAY_BELL();
			else {
				shiftdown_instruction(current_program_line);
				emma_display_program();
				program_to_be_saved = TRUE;
				emma_display_program_status();
			}
			break;
		case _del:
			shiftup_instruction(current_program_line);
			emma_display_program();
			program_to_be_saved = TRUE;
			emma_display_program_status();
			break;
		case _other:
			if (emma_get_instruction(current_char, current_program_line)) {
				program_to_be_saved = TRUE;
				emma_display_program_status();
				if (current_program_line != number_of_program_lines - 1) ++current_program_line;
			}
			break;
		default:
			DISPLAY_BELL();
			break;
		}
	}
	// 0 / Stop and exit - 1 / Clean screen - 2 / Run program
	return goon;
}
//------------------------------------------------------------------------------
