//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
#include "emma_prg_getchar.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define LEFT_PARENTHESIS	'('
#define RIGHT_PARENTHESIS	')'
#define	ADDITION			'+'
#define SUBSTRACTION		'-'
#define MULTIPLICATION		'*'
#define DIVISION			'/'
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	int stack[EXPRESSION_MAX_SIZE + 1];
	int top;
} s_stack;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
// Function to find precedence of  operators.
static int precedence(char op) {
	if(op == '+'||op == '-') return 1;
	if(op == '*'||op == '/') return 2;
	return 0;
}

// Function to perform arithmetic operations.
static int applyOp( int a, int b, char op ) {
	switch (op) {
	case '+':
		return a + b;
	case '-':
		return a - b;
	case '*':
		return a * b;
	case '/':
		return a / b;
	}
	// never reaches
	return 0;
}
//------------------------------------------------------------------------------
// Stack primitives
//------------------------------------------------------------------------------
static void init( s_stack *s ) {
	for (size_t i = 0; i < EXPRESSION_MAX_SIZE; i++) s->stack[i] = 0;
	s->top = -1;
}
//------------------------------------------------------------------------------
static boolean empty( s_stack *stack ) {
	if (stack->top == -1) return TRUE;
	else return FALSE;
}
//------------------------------------------------------------------------------
static boolean full( s_stack *stack ) {
	if (stack->top == EXPRESSION_MAX_SIZE) return TRUE;
	else return FALSE;
}
//------------------------------------------------------------------------------
static void push( s_stack *stack, int data ) {
	if (!full(stack)) {
		stack->top = stack->top + 1;
		stack->stack[stack->top] = data;
	} else
		emma_fatal("%s", "(Expression evaluation) Could not insert data, Stack is full.");
}
//------------------------------------------------------------------------------
static int top( s_stack *stack ) {
	return stack->stack[stack->top];
}
//------------------------------------------------------------------------------
static int pop( s_stack *stack) {
	int data = 0;
	if (!empty(stack)) {
		data = stack->stack[stack->top];
		stack->top = stack->top - 1;
	} else
		emma_fatal("%s", "(Expression evaluation) Could not retrieve data, Stack is empty.");
	return data;
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
boolean emma_read_expression( char *expression ) {
	char *pt = expression;
	int c, current_char;
	boolean end = FALSE;
	*pt = '\0';
	while (! end) {
		switch ((c = emma_getchar(&current_char))) {
		case _cr:
		case _tab:
			if (strlen(expression) == 0) return FALSE;
			return TRUE;
			break;
		case _bs:
		case _del:
			if (strlen(expression) == 0)
				return FALSE;
			else {
				fprintf(stdout, "\b \b");
				*(--pt) = '\0';
			}
			break;
		case _space:
			break;
		case _other:
			if (isdigit(current_char) || isalpha(current_char)
			        || current_char == ADDITION || current_char == SUBSTRACTION || current_char == MULTIPLICATION || current_char == DIVISION
			        || current_char == LEFT_PARENTHESIS || current_char == RIGHT_PARENTHESIS) {
				if (isalpha(current_char))
					current_char = toupper(current_char);
				*pt = (char) current_char;
				*(++pt) = '\0';
				fprintf(stdout, "%c", current_char);
			} else
				DISPLAY_BELL();
			break;
		default:
			DISPLAY_BELL();
			break;
		}
		if (end && *expression != '\0') {
			if (! (end = emma_check_expression(expression)))
				DISPLAY_BELL();
		}
	}
	return TRUE;
}
//------------------------------------------------------------------------------
char *emma_get_expression_from_string( char **s ) {
	while (**s == SPACE || **s == TAB) ++*s;
	char *w = *s;
	boolean end = FALSE;
	while (! end) {
		switch (**s) {
		case TAB:
		case SPACE:
			if (strncmp(*s+1, LGDEP_KEYWORD_STEPS, strlen(LGDEP_KEYWORD_STEPS)) == 0
			        || strncmp(*s+1, LGDEP_KEYWORD_TIMES, strlen(LGDEP_KEYWORD_TIMES)) == 0
			        || strncmp(*s+1, LGDEP_KEYWORD_TO, strlen(LGDEP_KEYWORD_TO)) == 0)
				end = TRUE;
			else
				++*s;
			break;
		default:
			if (isdigit(**s) || isalpha(**s)
			        || **s == ADDITION || **s == SUBSTRACTION || **s == MULTIPLICATION || **s == DIVISION
			        || **s == LEFT_PARENTHESIS || **s == RIGHT_PARENTHESIS)
				++*s;
			else
				end = TRUE;
			break;
		}
	}
	**s = '\0';
	++*s;
	return w;
}
//------------------------------------------------------------------------------
#define STATE_0			0
#define STATE_1			1
#define STATE_2			2
#define STATE_3			3

boolean emma_check_expression( char *expression) {
	size_t idx = 0;
	int parentheses_counter = 0;
	int state = STATE_0;
	boolean error = FALSE;
	while (idx < strlen(expression) && ! error) {
		if (expression[idx] == ' ') continue;
		switch (state) {
		case STATE_0:
			if (expression[idx] == LEFT_PARENTHESIS) {
				++parentheses_counter;
				state = STATE_0;
			} else {
				if (isdigit(expression[idx])) {
					state = STATE_1;
				} else {
					if (isalpha(expression[idx])) {
						state = STATE_2;
					} else
						error = TRUE;
				}
			}
			break;
		case STATE_1:
			if (isdigit(expression[idx])) {
				state = STATE_1;
			} else {
				if (expression[idx] == ADDITION || expression[idx] == SUBSTRACTION
				        || expression[idx] == MULTIPLICATION || expression[idx] == DIVISION) {
					state = STATE_0;
				} else {
					if (expression[idx] == RIGHT_PARENTHESIS) {
						--parentheses_counter;
						state = STATE_3;
					} else
						error = TRUE;
				}
			}
			break;
		case STATE_2:
			if (isalpha(expression[idx])) {
				state = STATE_2;
			} else {
				if (expression[idx] == ADDITION || expression[idx] == SUBSTRACTION
				        || expression[idx] == MULTIPLICATION || expression[idx] == DIVISION) {
					state = STATE_0;
				} else {
					if (expression[idx] == RIGHT_PARENTHESIS) {
						--parentheses_counter;
						state = STATE_3;
					} else
						error = TRUE;
				}
			}
			break;
		case STATE_3:
			if (expression[idx] == RIGHT_PARENTHESIS) {
				--parentheses_counter;
				state = STATE_3;
			} else {
				if (expression[idx] == ADDITION || expression[idx] == SUBSTRACTION
				        || expression[idx] == MULTIPLICATION || expression[idx] == DIVISION) {
					state = STATE_0;
				} else
					error = TRUE;
			}
			break;
		default:
			break;
		}
		++idx;
	}
	if (error)
		return FALSE;
	if (parentheses_counter != 0)
		return FALSE;
	return TRUE;
}
//------------------------------------------------------------------------------
// From https://www.geeksforgeeks.org/expression-evaluation/
//
//	1. While there are still tokens to be read in,
//	   1.1 Get the next token.
//	   1.2 If the token is:
//	       1.2.1 A number: push it onto the value stack.
//	       1.2.2 A variable: get its value, and push onto the value stack.
//	       1.2.3 A left parenthesis: push it onto the operator stack.
//	       1.2.4 A right parenthesis:
//	         1 While the thing on top of the operator stack is not a
//	           left parenthesis,
//	             1 Pop the operator from the operator stack.
//	             2 Pop the value stack twice, getting two operands.
//	             3 Apply the operator to the operands, in the correct order.
//	             4 Push the result onto the value stack.
//	         2 Pop the left parenthesis from the operator stack, and discard it.
//	       1.2.5 An operator (call it thisOp):
//	         1 While the operator stack is not empty, and the top thing on the
//	           operator stack has the same or greater precedence as thisOp,
//	           1 Pop the operator from the operator stack.
//	           2 Pop the value stack twice, getting two operands.
//	           3 Apply the operator to the operands, in the correct order.
//	           4 Push the result onto the value stack.
//	         2 Push thisOp onto the operator stack.
//	2. While the operator stack is not empty,
//	    1 Pop the operator from the operator stack.
//	    2 Pop the value stack twice, getting two operands.
//	    3 Apply the operator to the operands, in the correct order.
//	    4 Push the result onto the value stack.
//	3. At this point the operator stack should be empty, and the value
//	   stack should have only one value in it, which is the final result.

boolean emma_evaluate_expression( char *expression, int *result) {
	*result = 0;

	s_stack operands;
	s_stack operators;

	init(&operands);
	init(&operators);

	size_t i = 0;
	while (expression[i] != '\0') {
		if  (expression[i] == ' ') {
			++i;
			continue;
		}
		// Current token is an opening brace, push it to 'operators'
		if (expression[i] == '(') {
			push(&operators, expression[i]);
			++i;
			continue;
		}
		// Current token is a number, push it to stack for operands.
		if (isdigit(expression[i])) {
			int val = 0;
			while (i < strlen(expression) && isdigit(expression[i])) {
				val = (val * 10) + (expression[i ]- '0');
				++i;
			}
			push(&operands, val);
			continue;
		}
		// Current token is a variable, evaluate it and push it to stack for operands.
		if (isalpha(expression[i])) {
			char variable[VARIABLE_MAX_NUMBER + 1];
			int j = 0;
			while (i < strlen(expression) && j < VARIABLE_MAX_NUMBER && (isdigit(expression[i]) || isalpha(expression[i]))) {
				variable[j++] = expression[i++];
			}
			variable[j] = '\0';
			int val = 0;
			if (emma_get_variable_value(variable, &val)) {
				push(&operands, val);
				continue;
			}
			return FALSE;
		}
		// Closing brace encountered, solve entire brace.
		if (expression[i] == ')') {
			while (! empty(&operators) && top(&operators) != '(') {
				int val2 = pop(&operands);
				int val1 = pop(&operands);
				char op = (char) pop(&operators);
				push(&operands, applyOp(val1, val2, op));
			}
			// pop opening brace.
			(void) pop(&operators);
			++i;
			continue;
		}
		// Current token is an operator.
		// While top of 'operators' has same or greater precedence to current token, which
		// is an operator. Apply operator on top of 'operators' to top two elements in operands stack.
		while (!empty(&operators) && precedence((char) top(&operators)) >= precedence(expression[i])) {
			int val2 =pop(&operands);
			int val1 = pop(&operands);
			char op = (char) pop(&operators);
			push(&operands, applyOp(val1, val2, op));
		}
		// Push current token to 'operators'.
		push(&operators, expression[i]);
		++i;
	}
	// Entire expression has been parsed at this point, apply remaining operators to remaining operands.
	while (!empty(&operators)) {
		int val2 = pop(&operands);
		int val1 = pop(&operands);
		char op = (char) pop(&operators);
		push(&operands, applyOp(val1, val2, op));
	}
	// Top of 'operands' contains result, return it.
	*result = top(&operands);
	return TRUE;
}
//------------------------------------------------------------------------------
