//------------------------------------------------------------------------------
// Copyright (c) 2018, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: emma
// Application to teach young children the basics of programming.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "emma.h"
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define ProcessPseudoFileSystem	"/proc/self/exe"
#define IPCKEY					0				// IPC key for this serve
#define BOARD_TITLE				" E M M A "
#define TEMPO					10000
#define HOME_ROW				5
#define HOME_COL				3
#define START_COL				HOME_COL + 3
#define TOP_LINE				4

#define SOUND_PROCESS			"mpg123 --gain 100 --quiet"
#define CAR_CRASH				"Car-crash-sound-effect.mp3"
#define CAR_FAIL				"Car-startup-fail.mp3"

#ifdef FRENCH
#define LGDEP_HOUSE				"Maison"
#endif
#ifdef ENGLISH
#define LGDEP_HOUSE				"Home"
#endif
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	int row;
	int column;
} coordinates;
//------------------------------------------------------------------------------
// EXPORTABLE VARIABLES
//------------------------------------------------------------------------------
int 				nbLINES;
int 				nbCOLS;
char 				*screeninit;			// Startup terminal initialization
char 				*screendeinit;			// Exit terminal de-initialization
char 				*clear;					// Clear screen
char 				*move;					// Cursor positioning
coordinates 		square_coordinates;
e_direction			direction;
e_color 			background;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static int			board_request;	// request identificator for board
static int			board_response;	// response identificator for board
static s_msg		msg;			// IPC message
static char 		soundpath[PATH_MAX / 2 + 1];
static boolean		crash;

TRACYFD_DEFINITION
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void crashSound() {
	char cmd[PATH_MAX + 1];
	sprintf(cmd, "%s %s/%s", SOUND_PROCESS, soundpath, CAR_CRASH);
	int n = system(cmd);
}
static void failSound() {
	char cmd[PATH_MAX + 1];
	sprintf(cmd, "%s %s/%s", SOUND_PROCESS, soundpath, CAR_FAIL);
	int n = system(cmd);
}
//------------------------------------------------------------------------------
static void clear_square(int row, int column) {
	MOVE_CURSOR(row, column);
	fprintf(stdout, " ");
	fflush(stdout);
}
//------------------------------------------------------------------------------
static void trace_square(int row, int column) {
	MOVE_CURSOR(row, column);
	fprintf(stdout, ESC_COLOR " " ESC_ATTRIBUTSOFF, BACKGROUND(background));
	fflush(stdout);
}
//------------------------------------------------------------------------------
static void draw_square(int row, int column) {
	usleep(TEMPO);
	MOVE_CURSOR(row, column);
	fprintf(stdout, ESC_COLOR " " ESC_ATTRIBUTSOFF, BACKGROUND(red));
	fflush(stdout);
}
//------------------------------------------------------------------------------
static void draw_big_square(int row, int column) {
	MOVE_CURSOR(row - 1, column - 2);
	fprintf(stdout, ESC_COLOR "   " ESC_ATTRIBUTSOFF, BACKGROUND(red));
	MOVE_CURSOR(row, column - 2);
	fprintf(stdout, ESC_COLOR "   " ESC_ATTRIBUTSOFF, BACKGROUND(red));
	MOVE_CURSOR(row + 1, column - 2);
	fprintf(stdout, ESC_COLOR "   " ESC_ATTRIBUTSOFF, BACKGROUND(red));
	fflush(stdout);
}
//------------------------------------------------------------------------------
static void board_restart( void ) {
	MOVE_CURSOR(HOME_ROW - 2, HOME_COL - 2);
	CLEAR_SCREEN_FROM_CURSOR_DOWN();

	MOVE_CURSOR(HOME_ROW - 2, HOME_COL - 2);
	fprintf(stdout, LGDEP_HOUSE);
	MOVE_CURSOR(HOME_ROW -1, HOME_COL - 2);
	fprintf(stdout, "╭───╮");
	MOVE_CURSOR(HOME_ROW, HOME_COL - 2);
	fprintf(stdout, "│ " );
	draw_square(HOME_ROW,HOME_COL);
	fprintf(stdout, " │");
	MOVE_CURSOR(HOME_ROW + 1, HOME_COL - 2);
	fprintf(stdout, "╰───╯");

	fflush(stdout);

	square_coordinates.row = HOME_ROW;
	square_coordinates.column = HOME_COL;
	direction = HORIZONTAL_RIGHT;
}
//------------------------------------------------------------------------------
static void board_start( void ) {
	size_t n = ((size_t) nbCOLS - strlen(BOARD_TITLE)) / 2;
	size_t i;
	CLEAR_ENTIRE_SCREEN();
	HIDE_CURSOR();

	MOVE_CURSOR(1, 1);
	fprintf(stdout, ESC_COLOR ESC_STRONG_ON ESC_REVERSE_ON, FOREGROUND(yellow));
	for (i = 0; i < n; i++) fprintf(stdout, " ");
	fprintf(stdout, "%s", BOARD_TITLE);
	for (i = i + strlen(BOARD_TITLE); i < (size_t) nbCOLS; i++) fprintf(stdout, " ");
	fprintf(stdout,  ESC_ATTRIBUTSOFF);

	board_restart();
}
//------------------------------------------------------------------------------
static boolean go_forward( e_direction direction, int steps ) {
	switch (direction) {
	case HORIZONTAL_RIGHT:
		if (square_coordinates.row == HOME_ROW && square_coordinates.column == HOME_COL) {
			clear_square(square_coordinates.row, square_coordinates.column);
			square_coordinates.column = START_COL;
			draw_square(square_coordinates.row, ++square_coordinates.column);
			--steps;
		}
		for (int i = 0; i < steps; i++) {
			if (nbCOLS == square_coordinates.column + 1) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(square_coordinates.row, ++square_coordinates.column);
		}
		break;
	case HORIZONTAL_LEFT:
		if (square_coordinates.row == HOME_ROW && square_coordinates.column == HOME_COL) return FALSE;
		for (int i = 0; i < steps; i++) {
			if (0 == square_coordinates.column - 1) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(square_coordinates.row, --square_coordinates.column);
		}
		break;
	case VERTICAL_UP:
		for (int i = 0; i < steps; i++) {
			if (TOP_LINE == square_coordinates.row) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(--square_coordinates.row, square_coordinates.column);
		}
		break;
	case VERTICAL_DOWN:
		for (int i = 0; i < steps; i++) {
			if (nbLINES == square_coordinates.row) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(++square_coordinates.row, square_coordinates.column);
		}
		break;
	}
	return TRUE;
}
//------------------------------------------------------------------------------
static void change_background(int color) {
	background = color;
}
//------------------------------------------------------------------------------
static boolean go_backward( e_direction direction, int steps ) {
	crash = TRUE;
	switch (direction) {
	case HORIZONTAL_RIGHT:
		if (square_coordinates.row == HOME_ROW && square_coordinates.column == HOME_COL) {
			crash = FALSE;
			return FALSE;
		}
		for (int i = 0; i < steps; i++) {
			if (0 == square_coordinates.column - 1) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(square_coordinates.row, --square_coordinates.column);
		}
		break;
	case HORIZONTAL_LEFT:
		for (int i = 0; i < steps; i++) {
			if (nbCOLS == square_coordinates.column + 1) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(square_coordinates.row, ++square_coordinates.column);
		}
		break;
	case VERTICAL_UP:
		for (int i = 0; i < steps; i++) {
			if (nbLINES == square_coordinates.row) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(++square_coordinates.row, square_coordinates.column);
		}
		break;
	case VERTICAL_DOWN:
		for (int i = 0; i < steps; i++) {
			if (TOP_LINE == square_coordinates.row) return FALSE;
			trace_square(square_coordinates.row, square_coordinates.column);
			draw_square(--square_coordinates.row, square_coordinates.column);
		}
		break;
	}
	return TRUE;
}
//------------------------------------------------------------------------------
static boolean skip( e_direction direction, int steps ) {
	clear_square(square_coordinates.row, square_coordinates.column);
	switch (direction) {
	case HORIZONTAL_RIGHT:
		if (square_coordinates.row == HOME_ROW && square_coordinates.column == HOME_COL) {
			clear_square(square_coordinates.row, square_coordinates.column);
			square_coordinates.column = START_COL;
			--steps;
		}
		if (nbCOLS <= square_coordinates.column + steps) {
			square_coordinates.column = nbCOLS - 1;
			draw_square(square_coordinates.row, square_coordinates.column);
			return FALSE;
		}
		square_coordinates.column += steps;
		draw_square(square_coordinates.row, square_coordinates.column);
		break;
	case HORIZONTAL_LEFT:
		if (square_coordinates.row == HOME_ROW && square_coordinates.column == HOME_COL) return FALSE;
		if (0 >= square_coordinates.column - steps) {
			square_coordinates.column = 1;
			draw_square(square_coordinates.row, square_coordinates.column);
			return FALSE;
		}
		square_coordinates.column -= steps;
		draw_square(square_coordinates.row, square_coordinates.column);
		break;
	case VERTICAL_UP:
		if (TOP_LINE >= square_coordinates.row - steps) {
			square_coordinates.row = TOP_LINE + 1;
			draw_square(square_coordinates.row, square_coordinates.column);
			return FALSE;
		}
		square_coordinates.row -= steps;
		draw_square(square_coordinates.row, square_coordinates.column);
		break;
	case VERTICAL_DOWN:
		if (nbLINES <= square_coordinates.row + steps) {
			square_coordinates.row = nbLINES - 1;
			draw_square(square_coordinates.row, square_coordinates.column);
			return FALSE;
		}
		square_coordinates.row += steps;
		draw_square(square_coordinates.row, square_coordinates.column);
		break;
	}
	return TRUE;
}
//------------------------------------------------------------------------------
static void windowsizemanager( int sig ) {
	signal(SIGWINCH, SIG_IGN);
	emma_getTerminalSize(&nbLINES, &nbCOLS);
	board_start();
	signal(SIGWINCH, windowsizemanager);
}
//------------------------------------------------------------------------------
static void interruptmanager( int sig ) {
	signal(SIGHUP, SIG_IGN);
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
//	signal(SIGKILL, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	emma_cleanContext();
	exit(EXIT_FAILURE);
}
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
void emma_cleanContext( void ) {
	// Restore screen
	emma_endScreen();
	// IPC deletion
	DELETEIPC(board_request);
	DELETEIPC(board_response);
	// Signals
	signal(SIGHUP, SIG_DFL);
	signal(SIGABRT, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGINT, SIG_DFL);
//	signal(SIGKILL, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGWINCH, SIG_DFL);
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//------------------------------------------------------------------------------
int main( int argc, char **argv ) {
	char param[MSG_MAX_SIZE + 1];		// param part of a message
	char key[KEY_MAX_SIZE + 1];			// key part of a message
	int additional_information;

	_TRACY_START(_TRACYBOARD);
	// Signals
	signal(SIGHUP, SIG_IGN);
	signal(SIGABRT, SIG_IGN);
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);
//	signal(SIGKILL, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGWINCH, SIG_IGN);
	// Get rows and cols number of the terminal
	int opt;
	while ((opt = getopt(argc, argv, "r:c:")) != -1) {
		switch (opt) {
		case 'r':
			nbLINES = atoi(optarg);
			break;
		case 'c':
			nbCOLS = atoi(optarg);
			break;
		default:
			emma_fatal("Usage: %s -r rows -c columns\n", argv[0]);
		}
	}
	if (nbLINES == 0 || nbCOLS == 0)
		emma_fatal("Usage: %s -r rows -c columns\n", argv[0]);
	// Initialize screen
	emma_initScreen();
	background = white;
	// IPC Creation
	CREATEIPC(BOARD, PROGRAM, board_request, BOARD_REQUEST);
	CREATEIPC(BOARD, PROGRAM, board_response, BOARD_RESPONSE);
	// Interrupts signals
	signal(SIGHUP, interruptmanager);
	signal(SIGABRT, interruptmanager);
	signal(SIGTERM, interruptmanager);
	signal(SIGINT, interruptmanager);
//	signal(SIGKILL, interruptmanager);
	signal(SIGQUIT, interruptmanager);
	// Window change size signal management
	signal(SIGWINCH, windowsizemanager);
	// Processing loop
	while (1) {
		READIPC(PROGRAM, board_request);
		// Split text message in 2 parts: key and additional information. Both
		// parts are separated by character ':'
		emma_splitMsg(msg.mtext, key, &additional_information);

		// START
		if (strncmp(key, BOARD_MSG_START, MSG_MAX_SIZE) == 0) {
			//----- Get real path of this executable to compute the path of sound files
			// /proc/self is a symbolic link to the process-ID subdir
			// of /proc, e.g. /proc/4323 when the pid of the process
			// of this program is 4323.
			// Inside /proc/<pid> there is a symbolic link to the
			// executable that is running as this <pid>.  This symbolic
			// link is called "exe".
			// So if we read the path where the symlink /proc/self/exe
			// points to we have the full path of the executable.
			if (readlink(ProcessPseudoFileSystem, soundpath, sizeof(soundpath)) == -1) {
				SEND_BACK(ERROR_CPPOTCE);
				continue;
			}
			dirname(soundpath);
			strcat(soundpath, SOUNDDIR);
			if (! emma_existDir(soundpath)) {
				SEND_BACK(ERROR_SDDNE);
				continue;
			}
			board_start();
			SEND_BACK(ACKNOWLEDGMENT);
			continue;
		}

		// STOP
		if (strncmp(key, BOARD_MSG_STOP, MSG_MAX_SIZE) == 0) {
			SEND_BACK(ACKNOWLEDGMENT);
			break;
		}

		// END
		if (strncmp(key, BOARD_MSG_END, MSG_MAX_SIZE) == 0) {
			draw_big_square(square_coordinates.row, ++square_coordinates.column);
			SEND_BACK(ACKNOWLEDGMENT);
			continue;
		}

		// RESTART / HOME
		if (strncmp(key, BOARD_MSG_RESTART, MSG_MAX_SIZE) == 0
		        || strncmp(msg.mtext, BOARD_MSG_HOME, MSG_MAX_SIZE) == 0) {
			board_restart();
			SEND_BACK(ACKNOWLEDGMENT);
			continue;
		}

		// TURNRIGHT
		if (strncmp(key, BOARD_MSG_TURNRIGHT, MSG_MAX_SIZE) == 0) {
			switch (direction) {
			case HORIZONTAL_RIGHT:
				direction = VERTICAL_DOWN;
				break;
			case HORIZONTAL_LEFT:
				direction = VERTICAL_UP;
				break;
			case VERTICAL_UP:
				direction = HORIZONTAL_RIGHT;
				break;
			case VERTICAL_DOWN:
				direction = HORIZONTAL_LEFT;
				break;
			}
			SEND_BACK(ACKNOWLEDGMENT);
			continue;
		}

		// TURNLEFT
		if (strncmp(key, BOARD_MSG_TURNLEFT, MSG_MAX_SIZE) == 0) {
			switch (direction) {
			case HORIZONTAL_RIGHT:
				direction = VERTICAL_UP;
				break;
			case HORIZONTAL_LEFT:
				direction = VERTICAL_DOWN;
				break;
			case VERTICAL_UP:
				direction = HORIZONTAL_LEFT;
				break;
			case VERTICAL_DOWN:
				direction = HORIZONTAL_RIGHT;
				break;
			}
			SEND_BACK(ACKNOWLEDGMENT);
			continue;
		}

		// FORWARD
		if (strncmp(key, BOARD_MSG_FORWARD, MSG_MAX_SIZE) == 0) {
			if (go_forward(direction, additional_information)) {
				SEND_BACK(ACKNOWLEDGMENT);
			} else {
				SEND_BACK(WARNING_CRASH);
				crashSound();
			}
			continue;
		}

		// BACKWARD
		if (strncmp(key, BOARD_MSG_BACKWARD, MSG_MAX_SIZE) == 0) {
			if (go_backward(direction, additional_information)) {
				SEND_BACK(ACKNOWLEDGMENT);
			} else {
				SEND_BACK(WARNING_CRASH);
				if (crash) crashSound();
				else failSound();
			}
			continue;
		}

		// SKIP
		if (strncmp(key, BOARD_MSG_SKIP, MSG_MAX_SIZE) == 0) {
			if (skip(direction, additional_information)) {
				SEND_BACK(ACKNOWLEDGMENT);
			} else {
				SEND_BACK(WARNING_CRASH);
				crashSound();
			}
			continue;
		}
		// COLOR
		if (strncmp(key, BOARD_MSG_COLOR, MSG_MAX_SIZE) == 0) {
			change_background(additional_information);
			SEND_BACK(ACKNOWLEDGMENT);
			continue;
		}
	}
	//-- Exit
	emma_cleanContext();
	_TRACY_STOP();
	return(EXIT_SUCCESS);
}
//------------------------------------------------------------------------------
