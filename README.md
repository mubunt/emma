# *emma*, an application to teach young children the basics of programming.

<img align="left" src="./icons/emma.png">

**emma** is an application to support the learning of programming to young children (6, 7 years). It introduces the notion of *instruction*, *expression*, named memory cell (*variables* in other words) and *command*. Its primitive look and feel is intended to prevent the children from the excesses of the graphics and the mouse (repeated clicks, copy-paste inopportune, etc.).

To interest the child, the goal of any **emma** program  entered in the window "PROGRAM" is to move a small red square in the window "EXECUTION". While moving, the square leaves a trace. The child must therefore try to draw a picture.

Note: _Not sure that this application offers of great interest but it was an opportunity to handle the System 5 IPCs, to use ESCAPE characters for terminal programming, and especially to program in C. It is certain that in JAVA, it would have another look (and would be much easier) but that's it._

***...TO BE COMPLETED...***


## EXAMPLES
![Example 1](./README_images/emma-01.png  "Example 1")

![Example 2](./README_images/emma-02.png  "Example 2")

![Example 3](./README_images/emma-03.png  "Example 3")

![Example 4](./README_images/emma-04.png  "Example 4")

## LICENSE
**emma** is covered by the GNU General Public License (GPL) version 3 and above.

## INSTRUCTIONS

| French                 | English                |
|------------------------|------------------------|
| Avancer *n* pas        | Move forward *n* steps |
| Reculer *n* pas        | Go back *n* steps      |
| Sauter *n* pas         | Skip *n* steps         |
| Tourner gauche         | Turn left              |
| Tourner Droite         | Turn right             |
| Colorer en *couleur*   | Color in *color*       |
| Faire *n* fois ... Fin | Do *n* times ... End   |

*n* is an integer arithmetic expression. Allowed binary operators are *+*, *-*, *\**; and */*. No unary operator. Parenthesis are also allowed.

*color* are basic ones in Linux terminal: black, white, red, blue, yellow, gree, magenta and cyan.

## COMMANDS

| Key | French       | English   |
|-----|--------------|-----------|
| F2  | Executer     | Execute   |
| F4  | Nettoyer     | Clean     |
| F6  | Enregistrer  | Save      |
| F7  | Reutiliser   | Load      |
| F9  | Quitter      | Exit      |

## USAGE
``` bash
$ emma -h
Usage: emma [ -V | -h ]
	with:
		-V: Display version and exit
		-h: Display help and exit

$ emma -V
EMMA - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
EMMA - Application to teach young children the basics of programming
EMMA - Version 0.4.0

$ emma

```
## STRUCTURE OF THE APPLICATION
This section walks you through **emma**'s structure. Once you understand this structure, you will easily find your way around in **emma**'s code base.

``` bash
$ yaTree
./                                  # Application level
├── README_images/                  # Images for documentation
│   ├── emma-01.png                 # 
│   ├── emma-02.png                 # 
│   ├── emma-03.png                 # 
│   ├── emma-04.png                 # 
│   └── emma-launcher.png           # 
├── emma-programs/                  # Examples of emma programs
│   ├── p1.emma                     # 
│   ├── p2.emma                     # 
│   ├── p3.emma                     # 
│   ├── p4.emma                     # 
│   ├── p5.emma                     # 
│   ├── p6.emma                     # 
│   ├── p7.emma                     # 
│   └── p8.emma                     # 
├── icons/                          # Icons directory
│   └── emma.png                    # 
├── sounds/                         # Sounds directory (mp3 files)
│   ├── Car-crash-sound-effect.mp3  # 
│   ├── Car-startup-fail.mp3        # 
│   └── Makefile                    # Makefile
├── src/                            # Source directory
│   ├── board/                      # Emma board window sources
│   │   ├── Makefile                # Makefile
│   │   └── emma_board.c            # emma board C source file
│   ├── lib/                        # Emma Program-Board common functions sources
│   │   ├── Makefile                # Makefile
│   │   └── emma_funcs.c            # General functions common to 'main' and 'board'
│   ├── main/                       # Emma main sources
│   │   ├── Makefile                # Makefile
│   │   └── emma.c                  # Emma main program
│   ├── program/                    # Emma program window sources
│   │   ├── Makefile                # Makefile
│   │   ├── emma_prg_confirmation.c # Emma program: save or not confirmation
│   │   ├── emma_prg_display.c      # Emma program: display routines
│   │   ├── emma_prg_editor.c       # Emma program: instruction editor
│   │   ├── emma_prg_executor.c     # Emma program: execution machine
│   │   ├── emma_prg_expression.c   # Emma program: expression processing
│   │   ├── emma_prg_file.c         # Emma program: file operation (load, save)
│   │   ├── emma_prg_getchar.c      # Emma program: character acquisition processing
│   │   ├── emma_prg_getchar.h      # Emma program: character acquisition header file
│   │   ├── emma_prg_instructions.c # Emma program: instruction processing routines
│   │   ├── emma_prg_main.c         # Emma program: main program
│   │   └── emma_prg_variables.c    # Emma program: variable processing
│   ├── Makefile                    # Makefile
│   ├── emma.h                      # Emma global header file
│   └── emma_language_dependent.h   # Texts depending on the language of use
├── COPYING.md                      # GNU General Public License
├── LICENSE.md                      # License file
├── Makefile                        # Makefile
├── README.md                       # ReadMe Markdown file
├── RELEASENOTES.md                 # Release Notes Mark-Down file
└── VERSION                         # Version identification text file

9 directories, 44 files
$
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd emma
$ make clean all
```
The application is, by default, in French. To generate the English version, modify the *emma/Makefile* file:
```Makefile
LANGUAGE	= ENGLISH
```

To add a new language, update the following files: *emma/Makefile*, *emma/src/emma_language_dependent.h*, *emma/src/program/emma_prg_main.c*,
*emma/src/program/emma_prg_instructions.c* and *emma/src/board/emma_board.c*.

## HOW TO INSTALL AND USE THIS APPLICATION
```Shell
$ cd emma
$ make release
    # Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
    # We consider that $BIN_DIR is a part of the PATH.
$ emma -V
EMMA - Copyright (c) 2018, Michel RIZZO. All Rights Reserved.
EMMA - Application to teach young children the basics of programming
EMMA - Version 0.4.0

$ emma
```
## NOTES
  - Terminals launched by **emma** are *xfce4-terminal*. To change, edit the source file *src/main/emma.c* and rebuild the application.
  - The values of the function keys used by the application are as follows:

| KEY | VALUE  |
|-----|--------|
| F1  | ^[OP   |
| F2  | ^[OQ   |
| F3  | ^[OR   |
| F4  | ^[OS   |
| F5  | ^[[15~ |
| F6  | ^[[17~ |
| F7  | ^[[18~ |
| F8  | ^[[19~ |
| F9  | ^[[20~ |
| F10 | ^[[21~ |
| F11 | ^[[23~ |
| F12 | ^[[24~ |

  - in *icons* directory, image comes from http://registerforbetting.com. This image can be used as icon if you create a shortcut on your desktop.
![Launcher](./README_images/emma-launcher.png  "Launcher")
  - The grammar of expressions accepted by **emma** is the following:

```
    expression ::= expression + expression
           | expression - expression
           | expression × expression
           | expression / expression
           | (expression)
           | variable
           | numeric
    numeric ::= digit numeric | digit
    digit ::= 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9
    variable ::= letter variable | letter
    letter ::= A | B ... Y | Z
```

## SOFTWARE REQUIREMENTS
- *mpg321*, the simple and lightweight command line MP3 player.
- Developped and tested on XUBUNTU 18.04, GCC v7.3.0.

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***
