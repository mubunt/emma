#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	Data file processing for C and C/Java projects
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		DATADIR			Path from $(BIN_DIR) where to install data files
#		DATA_R 			List of released read-only (440) data files
#		DATA_RW 		List of released read-write (640) data files
#		DATA_RWX 		List of released executable (750) data files
#------------------------------------------------------------------------------
MKDIR		= mkdir -p
RMDIR		= rm -fr
INSTALL		= install -p -v -D
RM 			= rm -f
#- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
all clean astyle cppcheck:
	@echo "-- $@: Nothing to do";
install:
	$(MUTE)$(MKDIR) $(BIN_DIR)/$(DATADIR)
	$(MUTE)if [ ! -z "$(DATA_R)" ]; then \
		echo "-- Copying DATA (read only) files to $(BIN_DIR)/$(DATADIR)"; \
		$(INSTALL) -m 440 $(DATA_R) $(BIN_DIR)/$(DATADIR); \
	fi
	$(MUTE)if [ ! -z "$(DATA_RW)" ]; then \
		echo "-- Copying DATA (read - write) files to $(BIN_DIR)/$(DATADIR)"; \
		$(INSTALL) -m 640 $(DATA_RW) $(BIN_DIR)/$(DATADIR); \
	fi
	$(MUTE)if [ ! -z "$(DATA_RWX)" ]; then \
		echo "-- Copying DATA (read - write - execute) files to $(BIN_DIR)/$(DATADIR)"; \
		$(INSTALL) -m 750 $(DATA_RWX) $(BIN_DIR)/$(DATADIR); \
	fi
cleaninstall:
	$(MUTE)if [ ! -z "$(DATA_R)" ]; then \
		echo "-- Removing DATA (read only) files from $(BIN_DIR)/$(DATADIR)"; \
		$(RM) $(addprefix $(BIN_DIR)/$(DATADIR),$(DATA_R)); \
	fi
	$(MUTE)if [ ! -z "$(DATA_RW)" ]; then \
		echo "-- Removing DATA (read - write) files from $(BIN_DIR)/$(DATADIR)"; \
		$(RM) $(addprefix $(BIN_DIR)/$(DATADIR),$(DATA_RW)); \
	fi
	$(MUTE)if [ ! -z "$(DATA_RWX)" ]; then \
		echo "-- Removing DATA (read - write - execute) files from $(BIN_DIR)/$(DATADIR)"; \
		$(RM) $(addprefix $(BIN_DIR)/$(DATADIR),$(DATA_RWX)); \
	fi
#-------------------------------------------------------------------------------
.PHONY: all clean install cleaninstall astyle cppcheck
#-------------------------------------------------------------------------------
