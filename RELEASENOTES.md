# RELEASE NOTES: *emma*, an application to teach young children the basics of programming.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 1.1.15**:
  - Updated build system components.

- **Version 1.1.14**:
  - Updated build system.

- **Version 1.1.13**:
  - Removed unused files.

- **Version 1.1.12**:
  - Updated build system component(s)

- **Version 1.1.11**:
  - Reworked build system to ease global and inter-project updated.
  - Run *astyle* on sources.
  - Abandonned Windows support.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.1.10**:
  - Some minor changes in .comment file(s).

- **Version 1.1.9**:
  - Standardization of the installation of executables and libraries in $BIN_DIR, $LIB_DIR anetd $INC_DIR defined in the environment.

- **Version 1.1.8**:
  - Fixed "clean" issue (again!) in ./Makefile.

- **Version 1.1.7**:
  - Fixed "clean" issue in ./src/Makefile.

 - **Version 1.1.6**:
  - Fixed ./Makefile, ./src/Makefile and ./src/.../Makefile.

- **Version 1.1.5**:
  - Debugged and fixed bug on crash in emma_program after moving to UBUNTU 18.10: Since the migration to Ubuntu 18.10, it seems that the executable activated via a terminal itself activated via a "fork / exec" is no longer able to acquire the number of rows and columns of the said terminal (result of ioctl (TIOCGWINSZ) = OK and ws_row = ws_col = 0). It should be noted, however, that the terminal characteristics are correct after a "SIGWINCH" event. To overcome this anomaly, and for short, the initial dimensions of the terminal are passed as arguments to the call.
  - Fixed compilation warnings appeared with GCC v8.2.
- 
**Version 1.1.4**:
  - Fixed bug in *src/Makefile*.

- **Version 1.1.3**:
  - Added tagging of new release.

- **Version 1.1.2**:
  - Improved Makefiles: version identification, optimization options (debug, release).
  - Added version identification text file.

- **Version 1.1.1**:
  - Fixed typos in this file.
  - Improved *program* window cancelling.
  - Improved Makefile writing...

- **Version 1.1.0**:
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Reworked a little bit the main program (*src/main/emma.c*) to be able to run via a shortcut without an attached terminal.
  - Reworked application error display; it was a non-sense to display them on stdout because the host terminal is closed immediately after!
  - Fixed depandency issue in *src/board/Makefile* and *src/progral/Makefile* on *libemma.a*.
  - Enhanced interrupts processing (*board* and *program*).
  - Some others improvements...

- **Version 1.0.0**:
  - Moved from GPL v2 to GPL v3.
  - Added support of simple variables.
  - Added "COLOR" instruction.
  - Clean string size declaration.
  - Run valgrind on *emma_main* and *emma_program* (*valgrind --leak-check=full --track-origins=yes --show-leak-kinds=all*).
  - Added window size change management.

- **Version 0.9.0**:
  - Renamed *src/program/emma_getchar.c* as *src/program/emma_prg_getchar.c*.
  - Added a specific head file to *src/program/emma_prg_getchar.c*.
  - Accepted integer arithmetic expressions in instructions instead of simple integer (*times* and *steps*).

*- *Version 0.8.0**:
  - Added the management of a short description (32 characters) of each saved program. These descriptions are displayed when requesting a program load.
  - Added the storage of the name and the description of the file loaded a memory to propose it during the next backup.

- **Version 0.7.0**:
  - Added English version.

- **Versions 0.1.0 to 0.6.0**:
  - Development versions to ignore.
